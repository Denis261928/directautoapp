package com.vallsoft.directionautoapp.repository;

import android.content.Context;
import android.content.SharedPreferences;

public class UserDataRepository {
    public static final String STORED_DATA = "phone_number";

    private static SharedPreferences preferences;

    public UserDataRepository(Context context) {
        preferences = context.getSharedPreferences(STORED_DATA, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor() {
        return preferences.edit();
    }

    public void setCode(String data) {
        getEditor().putString("code", data).commit();
    }

    public String getCode() {
        return preferences.getString("code", "");
    }

    public void setPhoneNumber(String data) {
        getEditor().putString("phone_number", data).commit();
    }

    public String getPhoneNumber() {
        return preferences.getString("phone_number", "");
    }

    public void setDeviceToken(String data) {
        getEditor().putString("token", data).commit();
    }

    public String getDeviceToken() {
        return preferences.getString("token", "");
    }
}
