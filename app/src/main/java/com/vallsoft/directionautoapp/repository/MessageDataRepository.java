package com.vallsoft.directionautoapp.repository;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.vallsoft.directionautoapp.model.MessageDataList;

public class MessageDataRepository {
    public static final String STORED_DATA = "message_data_list";

    private static SharedPreferences preferences;

    private Gson gson = new Gson();

    public MessageDataRepository(Context context) {
        preferences = context.getSharedPreferences(STORED_DATA, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor() {
        return preferences.edit();
    }

    public String getDescription() {
        return preferences.getString("description", "");
    }

    public void setDescription(String description) {
        getEditor().putString("description", description).commit();
    }

    public void setMessageDataList(MessageDataList messageDataList) {
        String messageDataListJson = gson.toJson(messageDataList);
        getEditor().putString("message_data_list", messageDataListJson).commit();
    }

    public MessageDataList getMessageDataList() {
        String messageDataListJson = preferences.getString("message_data_list", "");
        if (messageDataListJson.equals("")) {
            return null;
        }
        return gson.fromJson(messageDataListJson, MessageDataList.class);
    }

}
