package com.vallsoft.directionautoapp.dialog.question.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.model.api.Answer;

import java.util.List;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder>{
    private List<Answer> answerList;
    private OnAnswerListener listener;
    private boolean isQuestionAnswered;

    public QuestionAdapter(List<Answer> answerList, OnAnswerListener listener, boolean isQuestionAnswered) {
        this.answerList = answerList;
        this.listener = listener;
        this.isQuestionAnswered = isQuestionAnswered;
    }

    public void setAnswerList(List<Answer> answerList) {
        this.answerList = answerList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_answer, parent, false);

        return new QuestionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder holder, int position) {
        holder.bind(answerList.get(position), position, listener, isQuestionAnswered);
    }

    @Override
    public int getItemCount() {
        return answerList.size();
    }

    static class QuestionViewHolder extends RecyclerView.ViewHolder {
        private TextView answerText;
        private ImageView answerCheckboxIcon;

        private Answer answer;
        private int position;
        private OnAnswerListener listener;
        private boolean isQuestionAnswered;

        QuestionViewHolder(@NonNull View itemView) {
            super(itemView);

            answerText = itemView.findViewById(R.id.answer_text);
            answerCheckboxIcon = itemView.findViewById(R.id.answer_checkbox_icon);

            itemView.setOnClickListener(view -> answerClick());
        }

        void bind(Answer answer, int position, OnAnswerListener listener, boolean isQuestionAnswered) {
            this.answer = answer;
            this.position = position;
            this.listener = listener;
            this.isQuestionAnswered = isQuestionAnswered;
            answerText.setText(answer.getAnswer());
            if (answer.isMarked())
                answerCheckboxIcon.setImageResource(R.drawable.ic_checked);
            else
                answerCheckboxIcon.setImageResource(R.drawable.ic_unchecked);
        }

        void answerClick() {
            if (!isQuestionAnswered) {
                if (answer.isMarked()) {
                    answer.setMarked(false);
                    answerCheckboxIcon.setImageResource(R.drawable.ic_unchecked);
                } else {
                    answer.setMarked(true);
                    answerCheckboxIcon.setImageResource(R.drawable.ic_checked);
                }
                listener.onAnswerClick(answer, position);
            }
        }
    }

    public interface OnAnswerListener {
        void onAnswerClick(Answer answer, int position);
    }
}
