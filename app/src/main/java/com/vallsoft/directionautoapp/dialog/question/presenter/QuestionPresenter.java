package com.vallsoft.directionautoapp.dialog.question.presenter;

import android.util.Log;

import com.vallsoft.directionautoapp.api.NetworkService;
import com.vallsoft.directionautoapp.dialog.question.QuestionDialog;
import com.vallsoft.directionautoapp.dialog.question.contact.QuestionContact;
import com.vallsoft.directionautoapp.model.QuestionData;
import com.vallsoft.directionautoapp.model.api.Answer;
import com.vallsoft.directionautoapp.model.api.Answers;
import com.vallsoft.directionautoapp.model.api.PhoneCallResponse;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionPresenter implements QuestionContact.Presenter {
    private QuestionData questionData;
    private boolean isQuestionAnswered;

    private UserDataRepository repository;
    private QuestionContact.View view;

    public QuestionPresenter(QuestionContact.View view, UserDataRepository repository, QuestionData questionData,
                             boolean isQuestionAnswered) {
        this.view = view;
        this.repository = repository;
        this.questionData = questionData.clone();
        this.isQuestionAnswered = isQuestionAnswered;
    }

    @Override
    public void init() {
        view.showQuestion(questionData.getQuestion());
        view.showAnswers(questionData.getAnswerList());
        view.setChooseAnswersEnabled(false);
        setPollAsRead();
    }

    private void setPollAsRead() {
        if (questionData.isRead())
            return;
        NetworkService.getInstance().getPhoneCallApi().markPollAsRead("380"+repository.getPhoneNumber(),
                                                                      questionData.getQuestionCode())
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if (response.isSuccessful()) {
                            Log.d(QuestionDialog.TAG, "Result: " + response.body().getResult());
                        } else {
                            Log.d(QuestionDialog.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        Log.d(QuestionDialog.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void answerClick(Answer answer, int position) {
        if (isQuestionAnswered)
            return;

        if (questionData.isMultipleChoice()) {
            questionData.getAnswerList().get(position).setMarked(answer.isMarked());
        } else {
            List<Answer> newAnswerList = new ArrayList<>();
            for (Answer eachAnswer : questionData.getAnswerList()) {
                newAnswerList.add(new Answer(eachAnswer.getAnswer(), false));
            }
            newAnswerList.get(position).setMarked(answer.isMarked());
            questionData.setAnswerList(newAnswerList);
        }
        view.updateAnswers(questionData.getAnswerList());

        boolean checked = false;
        for (Answer eachAnswer : questionData.getAnswerList()) {
            if (eachAnswer.isMarked()) {
                checked = true;
                break;
            }
        }
        view.setChooseAnswersEnabled(checked);
    }

    @Override
    public void chooseAnswers() {
        if (isQuestionAnswered)
            return;

        List<String> checkedAnswerList = new ArrayList<>();
        for (Answer answer: questionData.getAnswerList()) {
            if (answer.isMarked()) {
                checkedAnswerList.add(answer.getAnswer());
            }
        }
        String[] answersArray = checkedAnswerList.toArray(new String[0]);
        Answers answers = new Answers("380"+repository.getPhoneNumber(), questionData.getQuestionCode(), answersArray);
        Log.d(QuestionDialog.TAG, "Answer list:" + answers);
        Log.d(QuestionDialog.TAG, "Checked answer array:" + Arrays.toString(answersArray));
        view.showProgress();
        NetworkService.getInstance().getPhoneCallApi().setPollAnswer(answers)
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if (response.isSuccessful()) {
                            view.hideProgress();
                            view.success();
                        } else {
                            view.hideProgress();
                            view.showErrorDialog();
                            Log.d(QuestionDialog.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        view.hideProgress();
                        view.showErrorDialog();
                        Log.d(QuestionDialog.TAG, t.getMessage());
                    }
                });
    }
}
