package com.vallsoft.directionautoapp.dialog.question.contact;

import com.vallsoft.directionautoapp.model.api.Answer;

import java.util.List;

public interface QuestionContact {
    interface View {
        void showQuestion(String question);
        void showAnswers(List<Answer> answerList);
        void updateAnswers(List<Answer> answerList);
        void setChooseAnswersEnabled(boolean enabled);
        void success();
        void showErrorDialog();
        void showProgress();
        void hideProgress();
    }
    interface Presenter {
        void init();
        void answerClick(Answer answer, int position);
        void chooseAnswers();
    }
}
