package com.vallsoft.directionautoapp.dialog.bottom_media;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.vallsoft.directionautoapp.R;

public class BottomMediaDialog extends BottomSheetDialogFragment {
    private BottomSheetListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_bottom_media, container, false);

        Button photoButton = view.findViewById(R.id.photo_button);
        Button videoButton = view.findViewById(R.id.video_button);

        photoButton.setOnClickListener(v -> {
            listener.onPhotoClick();
            dismiss();
        });

        videoButton.setOnClickListener(v -> {
            listener.onVideoClick();
            dismiss();
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement BottomSheetListener");
        }
    }

    public interface BottomSheetListener {
        void onPhotoClick();
        void onVideoClick();
    }
}
