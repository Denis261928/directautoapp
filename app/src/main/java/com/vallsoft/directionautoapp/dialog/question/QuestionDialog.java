package com.vallsoft.directionautoapp.dialog.question;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.activity.notification.NotificationActivity;
import com.vallsoft.directionautoapp.dialog.question.adapter.QuestionAdapter;
import com.vallsoft.directionautoapp.dialog.question.contact.QuestionContact;
import com.vallsoft.directionautoapp.dialog.question.presenter.QuestionPresenter;
import com.vallsoft.directionautoapp.model.QuestionData;
import com.vallsoft.directionautoapp.model.api.Answer;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import java.util.List;

public class QuestionDialog implements QuestionContact.View, QuestionAdapter.OnAnswerListener {
    public static final String TAG = "QuestionDialogLog";

    private Activity parentActivity;

    private Dialog questionDialog;
    private ProgressDialog progressDialog;
    private NestedScrollView scrollView;
    private TextView questionText;
    private Button chooseButton;
    private RecyclerView answerRecyclerView;

    private QuestionAdapter adapter;
    private QuestionPresenter presenter;
    private boolean isQuestionAnswered;

    public QuestionDialog(Activity parentActivity, UserDataRepository repository, QuestionData questionData, boolean isQuestionAnswered) {
        this.parentActivity = parentActivity;
        this.presenter = new QuestionPresenter(this, repository, questionData, isQuestionAnswered);
        this.isQuestionAnswered = isQuestionAnswered;
    }

    public void showDialog() {
        questionDialog = new Dialog(parentActivity);
        questionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        questionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        questionDialog.setCancelable(false);
        questionDialog.setContentView(R.layout.dialog_question);

        questionText = questionDialog.findViewById(R.id.question_text);
        Button cancelButton = questionDialog.findViewById(R.id.cancel_button);
        chooseButton = questionDialog.findViewById(R.id.choose_button);
        answerRecyclerView = questionDialog.findViewById(R.id.answer_list);
        answerRecyclerView.setLayoutManager(new LinearLayoutManager(parentActivity.getApplicationContext()));

        scrollView = questionDialog.findViewById(R.id.scroll_view);

        presenter.init();

        cancelButton.setOnClickListener(view -> questionDialog.dismiss());

        chooseButton.setOnClickListener(view -> {
            presenter.chooseAnswers();
        });
    }

    @Override
    public void showQuestion(String question) {
        questionText.setText(question);
    }

    @Override
    public void showAnswers(List<Answer> answerList) {
        adapter = new QuestionAdapter(answerList,this, isQuestionAnswered);
        answerRecyclerView.setAdapter(adapter);
        questionDialog.show();
        setScrollViewPadding();
    }

    private void setScrollViewPadding() {
        new Handler().postDelayed(() -> {
            int childHeight = answerRecyclerView.getHeight();
            boolean isScrollable = scrollView.getHeight() < childHeight + scrollView.getPaddingTop() + scrollView.getPaddingBottom();
            if (isScrollable) {
                int paddingInDp = 10;
                final float scale = parentActivity.getResources().getDisplayMetrics().density;
                int paddingInPx = (int) (paddingInDp * scale + 0.5f);
                scrollView.setPadding(paddingInPx, paddingInPx, paddingInPx, paddingInPx);
            }
        }, 100);
    }

    @Override
    public void updateAnswers(List<Answer> answerList) {
        adapter.setAnswerList(answerList);
    }

    @Override
    public void setChooseAnswersEnabled(boolean enabled) {
        if (enabled && !chooseButton.isEnabled()) {
            chooseButton.setEnabled(true);
            chooseButton.setBackgroundResource(R.drawable.background_button);
        }
        else if (!enabled && chooseButton.isEnabled()) {
            chooseButton.setEnabled(false);
            chooseButton.setBackgroundResource(R.drawable.background_button_disable);
        }
    }

    @Override
    public void success() {
        if (questionDialog != null)
            questionDialog.dismiss();

        if (parentActivity instanceof NotificationActivity) {
            NotificationActivity activity = (NotificationActivity) parentActivity;
            activity.refresh();
        }
    }

    @Override
    public void showErrorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(parentActivity).create();
        alertDialog.setTitle(parentActivity.getString(R.string.ALERT_ERROR_TITLE));
        alertDialog.setMessage(parentActivity.getString(R.string.ERROR_ANSWER_SENDING));
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, parentActivity.getString(R.string.CANCEL),
                new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                questionDialog.dismiss();
            }
        });
        alertDialog.show();    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage(parentActivity.getResources().getString(R.string.LOADING_TEXT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void onAnswerClick(Answer answer, int position) {
        presenter.answerClick(answer, position);
    }
}
