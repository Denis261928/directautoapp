package com.vallsoft.directionautoapp.helper;

public enum  FileType {
    IMAGE, VIDEO, FILE, AUDIO
}
