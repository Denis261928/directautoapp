package com.vallsoft.directionautoapp.helper;

public enum RequestMainPageStatus {
    DEFAULT, THERE_ARE_NOTIFICATIONS, PHONE_CALL_MADE, MESSAGE_CLICKED, NO_INTERNET
}
