package com.vallsoft.directionautoapp.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkSmsAuthService {
    private static NetworkSmsAuthService mInstance;
    private static final String BASE_URL = "https://api.turbosms.ua/";
    private Retrofit mRetrofit;

    private static OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();

    private NetworkSmsAuthService() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient.addInterceptor(logging);

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static NetworkSmsAuthService getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkSmsAuthService();
        }
        return mInstance;
    }

    public SmsAuthApi getSmsAuthApi() {
        return mRetrofit.create(SmsAuthApi.class);
    }

}
