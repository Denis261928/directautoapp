package com.vallsoft.directionautoapp.api;

import com.vallsoft.directionautoapp.model.api.Answers;
import com.vallsoft.directionautoapp.model.api.Notifications;
import com.vallsoft.directionautoapp.model.api.NotificationsCount;
import com.vallsoft.directionautoapp.model.api.PhoneCallResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface PhoneCallApi {

    @POST("/MobileApp/hs/MobileAPI/CallMeBack")
    Call<PhoneCallResponse> callMeBack(@Query("PhoneNumber") String phoneNumber);

    @Multipart
    @POST("/MobileApp/hs/MobileAPI/CallMeBackWithFile")
    Call<PhoneCallResponse> callMeBackWithFile(@Query("PhoneNumber") String phoneNumber,
                                               @Query("Text") String text,
                                               @Part MultipartBody.Part file);

    @POST("/MobileApp/hs/MobileAPI/CallMeBackWithFile")
    Call<PhoneCallResponse> callMeBackWithFile(@Query("PhoneNumber") String phoneNumber,
                                               @Query("Text") String text);

    @POST("/MobileApp/hs/MobileAPI/SetToken")
    Call<PhoneCallResponse> setToken(@Query("PhoneNumber") String phoneNumber,
                                     @Query("Token") String token);

    @POST("/MobileApp/hs/MobileAPI/ReceiveMessages")
    Call<Notifications> getNotificationsByPhoneNumber(@Query("PhoneNumber") String phoneNumber);

    @POST("/MobileApp/hs/MobileAPI/ChangePhoneNumber")
    Call<PhoneCallResponse> updatePhoneNumber(@Query("PhoneNumber") String phoneNumber,
                                              @Query("NewPhoneNumber") String newPhoneNumber);

    @POST("/MobileApp/hs/MobileAPI/SetPollAnswer")
    Call<PhoneCallResponse> setPollAnswer(@Body Answers answers);

    @POST("/MobileApp/hs/MobileAPI/MarkMessageAsRead")
    Call<PhoneCallResponse> markMessageAsRead(@Query("PhoneNumber") String phoneNumber,
                                              @Query("MessageDate") String date);

    @POST("/MobileApp/hs/MobileAPI/MarkPollAsRead")
    Call<PhoneCallResponse> markPollAsRead(@Query("PhoneNumber") String phoneNumber,
                                           @Query("QuestionCode") int questionCode);

    @POST("/MobileApp/hs/MobileAPI/GetCountOfNewMessages")
    Call<NotificationsCount> getCountOfNewMessages(@Query("PhoneNumber") String phoneNumber);

}
