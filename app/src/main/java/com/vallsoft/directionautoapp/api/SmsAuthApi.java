package com.vallsoft.directionautoapp.api;

import com.vallsoft.directionautoapp.model.api.SmsAuthResponse;

import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SmsAuthApi {

    @Headers({
            "Authorization: Basic a2e8ce4535364d133741d18a84d9d13e42ac2262"
    })
    @POST("/message/send.json")
    Call<SmsAuthResponse> smsAuthorization(@Query("recipients[0]") String phoneNumber,
                                           @Query("sms[sender]") String sender,
                                           @Query("sms[text]") String code);

}
