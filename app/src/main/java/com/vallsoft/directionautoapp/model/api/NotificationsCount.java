package com.vallsoft.directionautoapp.model.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationsCount implements Serializable {
    @SerializedName("Result")
    private String result;
    @SerializedName("Count")
    private int count;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "NotificationsCount{" +
                "result='" + result + '\'' +
                ", count=" + count +
                '}';
    }
}
