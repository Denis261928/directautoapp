package com.vallsoft.directionautoapp.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SmsAuthResponse {
    @SerializedName("response_code")
    private int responseCode;
    @SerializedName("response_status")
    private String responseStatus;
    @SerializedName("response_result")
    private List<SmsAuthResultResponse> responseResultList;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public List<SmsAuthResultResponse> getResponseResultList() {
        return responseResultList;
    }

    public void setResponseResultList(List<SmsAuthResultResponse> responseResultList) {
        this.responseResultList = responseResultList;
    }

    @Override
    public String toString() {
        return "SmsAuthResponse{" +
                "responseCode=" + responseCode +
                ", responseStatus='" + responseStatus + '\'' +
                ", responseResultList=" + responseResultList +
                '}';
    }
}
