package com.vallsoft.directionautoapp.model;

import java.io.Serializable;

public class NotificationMessage implements Serializable {
    private String title;
    private String message;
    private boolean isRead;
    private String messageDate;

    public NotificationMessage(String title, String message, boolean isRead, String messageDate) {
        this.title = title;
        this.message = message;
        this.isRead = isRead;
        this.messageDate = messageDate;
    }

    public NotificationMessage() { }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    @Override
    public String toString() {
        return "NotificationMessage{" +
                "title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", isRead=" + isRead +
                ", messageDate='" + messageDate + '\'' +
                '}';
    }
}
