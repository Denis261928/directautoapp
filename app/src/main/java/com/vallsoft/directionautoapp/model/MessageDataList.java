package com.vallsoft.directionautoapp.model;

import java.util.List;

public class MessageDataList {
    private List<MessageData> messageDataList;

    public MessageDataList(List<MessageData> messageDataList) {
        this.messageDataList = messageDataList;
    }

    public MessageDataList() { }

    public List<MessageData> getMessageDataList() {
        return messageDataList;
    }

    public void setMessageDataList(List<MessageData> messageDataList) {
        this.messageDataList = messageDataList;
    }

    @Override
    public String toString() {
        return "MessageDataList{" +
                "messageDataList=" + messageDataList +
                '}';
    }
}
