package com.vallsoft.directionautoapp.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class Answers {
    @SerializedName("PhoneNumber")
    private String phoneNumber;
    @SerializedName("QuestionCode")
    private int questionCode;
    @SerializedName("Answers")
    private String[] answers;

    public Answers(String phoneNumber, int questionCode, String[] answers) {
        this.phoneNumber = phoneNumber;
        this.questionCode = questionCode;
        this.answers = answers;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(int questionCode) {
        this.questionCode = questionCode;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Answers{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", questionCode=" + questionCode +
                ", answers=" + Arrays.toString(answers) +
                '}';
    }
}
