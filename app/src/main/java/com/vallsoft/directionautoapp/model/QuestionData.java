package com.vallsoft.directionautoapp.model;

import com.vallsoft.directionautoapp.model.api.Answer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QuestionData implements Serializable {
    private boolean isRead;
    private boolean multipleChoice;
    private int questionCode;
    private String question;
    private List<Answer> answerList;

    public QuestionData(boolean isRead, boolean multipleChoice, int questionCode, String question, List<String> answerList) {
        this.isRead = isRead;
        this.multipleChoice = multipleChoice;
        this.questionCode = questionCode;
        this.question = question;
        initListWithUncheckedAnswers(answerList);
    }

    public QuestionData(List<Answer> answerList, boolean multipleChoice, int questionCode, String question) {
        this.multipleChoice = multipleChoice;
        this.questionCode = questionCode;
        this.question = question;
        this.answerList = answerList;
    }

    public QuestionData() {
    }

    private void initListWithUncheckedAnswers(List<String> list) {
        answerList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            answerList.add(new Answer(list.get(i), false));
        }
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isMultipleChoice() {
        return multipleChoice;
    }

    public void setMultipleChoice(boolean multipleChoice) {
        this.multipleChoice = multipleChoice;
    }

    public int getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(int questionCode) {
        this.questionCode = questionCode;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Answer> answerList) {
        this.answerList = answerList;
    }

    public QuestionData clone(){
        List<Answer> answers = new ArrayList<>();
        for (Answer answer: answerList) {
            answers.add(new Answer(answer.getAnswer(), answer.isMarked()));
        }
        return new QuestionData(answers, multipleChoice, questionCode, question);
    }

    @Override
    public String toString() {
        return "QuestionData{" +
                "multipleChoice=" + multipleChoice +
                ", questionCode=" + questionCode +
                ", question='" + question + '\'' +
                ", answerList=" + answerList +
                '}';
    }
}
