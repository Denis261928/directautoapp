package com.vallsoft.directionautoapp.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notifications {
    @SerializedName("Messages")
    private List<NotificationData> notificationList;

    public List<NotificationData> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<NotificationData> notificationList) {
        this.notificationList = notificationList;
    }

    @Override
    public String toString() {
        return "Notifications{" +
                ", notificationList=" + notificationList +
                '}';
    }
}
