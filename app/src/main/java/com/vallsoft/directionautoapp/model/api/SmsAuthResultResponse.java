package com.vallsoft.directionautoapp.model.api;

import com.google.gson.annotations.SerializedName;

public class SmsAuthResultResponse {
    @SerializedName("message_id")
    private String messageId;
    @SerializedName("response_code")
    private int responseCode;
    @SerializedName("response_status")
    private String responseStatus;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    @Override
    public String toString() {
        return "SmsAuthResultResponse{" +
                "messageId='" + messageId + '\'' +
                ", responseCode=" + responseCode +
                ", responseStatus='" + responseStatus + '\'' +
                '}';
    }
}
