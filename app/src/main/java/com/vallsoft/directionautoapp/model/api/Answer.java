package com.vallsoft.directionautoapp.model.api;

import java.io.Serializable;

public class Answer implements Serializable {
    private String answer;
    private boolean marked;

    public Answer(String answer, boolean marked) {
        this.answer = answer;
        this.marked = marked;
    }

    public Answer() { }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    @Override
    public String toString() {
        return "AnswerData{" +
                "answer='" + answer + '\'' +
                ", marked=" + marked +
                '}';
    }
}
