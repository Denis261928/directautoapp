package com.vallsoft.directionautoapp.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationData {
    private String date;
    private String theme;
    @SerializedName("read")
    private boolean isRead;
    private String text;
    private String type;
    @SerializedName("multiple_choice")
    private boolean multipleChoice;
    @SerializedName("question_code")
    private int questionCode;
    @SerializedName("poll_question")
    private String pollQuestion;
    @SerializedName("poll_answers")
    private List<Answer> answerList;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isMultipleChoice() {
        return multipleChoice;
    }

    public void setMultipleChoice(boolean multipleChoice) {
        this.multipleChoice = multipleChoice;
    }

    public int getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(int questionCode) {
        this.questionCode = questionCode;
    }

    public String getPollQuestion() {
        return pollQuestion;
    }

    public void setPollQuestion(String pollQuestion) {
        this.pollQuestion = pollQuestion;
    }

    public List<Answer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Answer> answerList) {
        this.answerList = answerList;
    }

    @Override
    public String toString() {
        return "NotificationData{" +
                "date='" + date + '\'' +
                ", theme='" + theme + '\'' +
                ", isRead=" + isRead +
                ", text='" + text + '\'' +
                ", type='" + type + '\'' +
                ", multipleChoice=" + multipleChoice +
                ", questionCode=" + questionCode +
                ", pollQuestion='" + pollQuestion + '\'' +
                ", answerList=" + answerList +
                '}';
    }
}
