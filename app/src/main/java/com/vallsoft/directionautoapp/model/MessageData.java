package com.vallsoft.directionautoapp.model;

import com.vallsoft.directionautoapp.helper.FileType;

import java.io.File;

public class MessageData {
    private File file;
    private String audioPath;
    private boolean isAudioPlaying = false;
    private FileType fileType;

    // when add audio
    public MessageData(File file, String audioPath, FileType fileType) {
        this.file = file;
        this.audioPath = audioPath;
        this.fileType = fileType;
    }

    // when add file from file
    public MessageData(File file, FileType fileType) {
        this.file = file;
        this.fileType = fileType;
    }


    public MessageData() {
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public boolean isPlaying() {
        return isAudioPlaying;
    }

    public void setPlaying(boolean playing) {
        isAudioPlaying = playing;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    @Override
    public String toString() {
        return "MessageData{" +
                "file=" + file +
                ", audioPath='" + audioPath + '\'' +
                ", isAudioPlaying=" + isAudioPlaying +
                ", fileType=" + fileType +
                '}';
    }
}
