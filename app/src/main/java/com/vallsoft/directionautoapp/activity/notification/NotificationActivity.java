package com.vallsoft.directionautoapp.activity.notification;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vallsoft.directionautoapp.R;

import com.vallsoft.directionautoapp.activity.notification.adapter.NotificationAdapter;
import com.vallsoft.directionautoapp.activity.notification.presenter.NotificationPresenter;
import com.vallsoft.directionautoapp.activity.notification.view.NotificationView;
import com.vallsoft.directionautoapp.dialog.question.QuestionDialog;
import com.vallsoft.directionautoapp.model.QuestionData;
import com.vallsoft.directionautoapp.model.api.Answer;
import com.vallsoft.directionautoapp.model.api.NotificationData;
import com.vallsoft.directionautoapp.helper.RequestMainPageStatus;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import java.util.List;

public class NotificationActivity extends AppCompatActivity implements NotificationView, NotificationAdapter.OnNotificationListener {
    public static final String TAG = "NotificationActivityLog";

    private RelativeLayout notificationsEmptyBlock, notificationsNoInternetBlock;
    private RecyclerView recyclerView;

    private NotificationAdapter adapter;
    private UserDataRepository repository;
    private NotificationPresenter presenter;

    private ProgressDialog progressDialog;
    private Handler statusHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        notificationsEmptyBlock = findViewById(R.id.notifications_empty_block);
        notificationsNoInternetBlock = findViewById(R.id.notifications_no_internet_block);
        Button tryConnectToInternetButton = findViewById(R.id.try_again_button);

        recyclerView = findViewById(R.id.notifications_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        repository = new UserDataRepository(this);
        presenter = new NotificationPresenter(this, repository);
        presenter.getNotificationsCall(NotificationActivity.this);

        tryConnectToInternetButton.setOnClickListener(view -> presenter.getNotificationsCall(NotificationActivity.this));

        findViewById(R.id.close_activity).setOnClickListener(view -> onBackPressed());
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            presenter.getNotificationsCall(this);
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void setScreenStatusBackground(final RequestMainPageStatus status) {
        statusHandler.post(() -> {
            switch (status) {
                case DEFAULT:
                    notificationsEmptyBlock.setVisibility(View.VISIBLE);
                    notificationsNoInternetBlock.setVisibility(View.GONE);
                    break;
                case THERE_ARE_NOTIFICATIONS:
                    notificationsEmptyBlock.setVisibility(View.GONE);
                    notificationsNoInternetBlock.setVisibility(View.GONE);
                    break;
                case NO_INTERNET:
                    notificationsEmptyBlock.setVisibility(View.GONE);
                    notificationsNoInternetBlock.setVisibility(View.VISIBLE);
                    break;
            }
        });
    }

    @Override
    public void showNotifications(final List<NotificationData> list) {
        adapter = new NotificationAdapter(list, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onNotificationClick(NotificationData notificationData) {
        if (isQuestionPoll(notificationData)) {
            QuestionData questionData = new QuestionData();
            questionData.setRead(notificationData.isRead());
            questionData.setMultipleChoice(notificationData.isMultipleChoice());
            questionData.setQuestionCode(notificationData.getQuestionCode());
            questionData.setQuestion(notificationData.getPollQuestion());
            questionData.setAnswerList(notificationData.getAnswerList());

            QuestionDialog dialog = new QuestionDialog(this, repository, questionData, isQuestionAnswered(questionData));
            dialog.showDialog();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(notificationData.getTheme());
            alertDialog.setMessage(notificationData.getText());
            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.CLOSE), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
            presenter.makeMessageAsRead(notificationData.getDate(), notificationData.isRead());
        }
    }

    private boolean isQuestionPoll(NotificationData notificationData) {
        return notificationData.getType().equals("poll");
    }

    private boolean isQuestionAnswered(QuestionData questionData) {
        boolean answered = false;
        for (Answer answer : questionData.getAnswerList()) {
            if (answer.isMarked()) {
                answered = true;
                break;
            }
        }
        return answered;
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(NotificationActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.LOADING_TEXT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void refresh() {
        presenter.getNotificationsCall(this);
    }
}
