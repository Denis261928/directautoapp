package com.vallsoft.directionautoapp.activity.profile.view;

public interface ProfileView {
    String getPhoneNumber();
    void showNotificationsCount(int count);
    void hideNotificationsCount();
    void startNewActivity(Class<?> activityClass);
    void confirmSavingDialog();
    void inputIsIncompleteDialog();
    void hideKeyBoard();
    void setPhoneNumberText(String phoneNumber);
    void showLogoutConfirmAlert();
    void showProgress();
    void hideProgress();
}
