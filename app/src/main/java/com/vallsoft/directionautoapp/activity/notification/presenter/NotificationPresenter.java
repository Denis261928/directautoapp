package com.vallsoft.directionautoapp.activity.notification.presenter;

import android.content.Context;
import android.util.Log;

import com.vallsoft.directionautoapp.activity.main.MainActivity;
import com.vallsoft.directionautoapp.activity.notification.NotificationActivity;
import com.vallsoft.directionautoapp.activity.notification.view.NotificationView;
import com.vallsoft.directionautoapp.api.NetworkService;
import com.vallsoft.directionautoapp.utils.NetworkUtils;
import com.vallsoft.directionautoapp.helper.RequestMainPageStatus;
import com.vallsoft.directionautoapp.model.api.NotificationData;
import com.vallsoft.directionautoapp.model.api.Notifications;
import com.vallsoft.directionautoapp.model.api.PhoneCallResponse;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationPresenter {
    private NotificationView view;
    private UserDataRepository repository;

    public NotificationPresenter(NotificationView view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    public void makeMessageAsRead(String date, boolean isRead) {
        if (isRead)
            return;
        NetworkService.getInstance().getPhoneCallApi()
                .markMessageAsRead("380"+repository.getPhoneNumber(), date)
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if (response.isSuccessful()) {
                            Log.d(MainActivity.TAG, "Result: " + response.body().getResult());
                        } else {
                            Log.d(MainActivity.TAG, response.errorBody().toString());
                        }
                        getNotificationsCall(view.getContext());
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        Log.d(MainActivity.TAG, t.getMessage());
                    }
                });
    }

    public void getNotificationsCall(Context context) {
        if (!NetworkUtils.getNetworkConnection(context)) {
            view.setScreenStatusBackground(RequestMainPageStatus.NO_INTERNET);
            return;
        }
        view.showProgress();
        NetworkService.getInstance().getPhoneCallApi().getNotificationsByPhoneNumber("380"+repository.getPhoneNumber())
                .enqueue(new Callback<Notifications>() {
                    @Override
                    public void onResponse(Call<Notifications> call, Response<Notifications> response) {
                        if (response.isSuccessful()) {
                            Notifications notifications = response.body();
                            if (notifications.getNotificationList() != null && notifications.getNotificationList().size() != 0) {
                                Log.d(NotificationActivity.TAG, notifications.toString());
                                view.setScreenStatusBackground(RequestMainPageStatus.THERE_ARE_NOTIFICATIONS);

                                List<NotificationData> notificationDataList = new ArrayList<>();
                                for (int i = notifications.getNotificationList().size()-1; i >= 0; i--) {
                                    notificationDataList.add(notifications.getNotificationList().get(i));
                                }
                                view.showNotifications(notificationDataList);
                            } else {
                                view.setScreenStatusBackground(RequestMainPageStatus.DEFAULT);
                            }
                        } else {
                            view.setScreenStatusBackground(RequestMainPageStatus.DEFAULT);
                            Log.d(NotificationActivity.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<Notifications> call, Throwable t) {
                        view.hideProgress();
                        view.setScreenStatusBackground(RequestMainPageStatus.DEFAULT);
                        Log.d(NotificationActivity.TAG, t.getMessage());
                    }
                });
    }
}