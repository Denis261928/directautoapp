package com.vallsoft.directionautoapp.activity.auth.presenter;

import android.util.Log;

import com.vallsoft.directionautoapp.activity.auth.AuthActivity;
import com.vallsoft.directionautoapp.activity.auth.view.AuthView;
import com.vallsoft.directionautoapp.api.NetworkSmsAuthService;
import com.vallsoft.directionautoapp.model.api.SmsAuthResponse;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthPresenter implements Serializable {
    private AuthView authView;
    private UserDataRepository repository;

    private boolean isSmsCodeFragmentVisible = false;

    public AuthPresenter(AuthView authView, UserDataRepository repository) {
        this.authView = authView;
        this.repository = repository;
    }

    public void getCodeClicked() {
        repository.setPhoneNumber(getClearPhoneNumber(authView.getPhoneNumber()));
        authView.showProgress();

        int code = (int) (Math.random()*899999+100000);
        String codeString = String.valueOf(code);
        repository.setCode(codeString);
        Log.d(AuthActivity.TAG, codeString);

        repository.setCode(codeString);
        Log.d(AuthActivity.TAG, "Code: " + repository.getCode());
        Log.d(AuthActivity.TAG, "Phone number: 380" + repository.getPhoneNumber());

        NetworkSmsAuthService.getInstance().getSmsAuthApi().smsAuthorization("380"+repository.getPhoneNumber(), "DirectAuto", codeString)
                .enqueue(new Callback<SmsAuthResponse>() {
                    @Override
                    public void onResponse(Call<SmsAuthResponse> call, Response<SmsAuthResponse> response) {
                        if (response.isSuccessful()) {
                            SmsAuthResponse smsAuthResponse = response.body();
                            Log.d(AuthActivity.TAG, smsAuthResponse.toString());
                            if (smsAuthResponse.getResponseResultList().get(0).getResponseStatus().equals("OK")) {
                                authView.hideProgress();
                                authView.showSmsFragment();
                                isSmsCodeFragmentVisible = true;
                            } else {
                                authView.showErrorDialog();
                                authView.hideProgress();
                            }
                        }
                        else {
                            authView.hideProgress();
                            authView.showErrorDialog();
                            Log.d(AuthActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<SmsAuthResponse> call, Throwable t) {
                        authView.hideProgress();
                        authView.showErrorDialog();
                        Log.d(AuthActivity.TAG, t.getMessage());
                    }
                });
    }

    public void inputPhoneNumberChanged(boolean isEnabledCodeButton) {
        String phoneNumberText = getClearPhoneNumber(authView.getPhoneNumber());

        if (phoneNumberText.length() < 9 && isEnabledCodeButton) {
            if (isSmsCodeFragmentVisible) {
                repository.setPhoneNumber("");
                isSmsCodeFragmentVisible = false;
                authView.hideSmsFragment();
            }
            authView.setDisabledCodeButton();
        } else if (phoneNumberText.length() == 9 && !isEnabledCodeButton) {
            authView.setEnabledCodeButton();
        }
    }

    private static String getClearPhoneNumber(String phoneNumber) {
        return phoneNumber.replace(" ", "");
    }

    public void checkAuthorized() {
        if (repository.getPhoneNumber() == null || repository.getPhoneNumber().equals("")
                || repository.getDeviceToken() == null || repository.getDeviceToken().equals("")) {
            Log.d(AuthActivity.TAG, "You have to auth!");
        } else {
            authView.startMainActivity();
        }
    }

    public void setState() {
        if (repository.getPhoneNumber() != null && !repository.getPhoneNumber().equals("")
                && repository.getCode() != null && !repository.getCode().equals("")) {
            authView.setPhoneNumber(repository.getPhoneNumber());
            authView.showSmsFragment();
            isSmsCodeFragmentVisible = true;
        }
    }
}
