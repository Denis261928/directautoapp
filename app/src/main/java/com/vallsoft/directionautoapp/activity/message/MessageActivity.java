package com.vallsoft.directionautoapp.activity.message;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.coursion.freakycoder.mediapicker.galleries.Gallery;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.activity.custom_gallery.CustomGalleryActivity;
import com.vallsoft.directionautoapp.activity.file_chooser.utils.FileUtils;
import com.vallsoft.directionautoapp.activity.main.MainActivity;
import com.vallsoft.directionautoapp.activity.message.adapter.MessageDataAdapter;
import com.vallsoft.directionautoapp.api.NetworkService;
import com.vallsoft.directionautoapp.dialog.bottom_media.BottomMediaDialog;
import com.vallsoft.directionautoapp.helper.ChooserType;
import com.vallsoft.directionautoapp.helper.FileType;
import com.vallsoft.directionautoapp.utils.ZipArchiver;
import com.vallsoft.directionautoapp.model.MessageData;
import com.vallsoft.directionautoapp.activity.message.helper.MessageDataCallBack;
import com.vallsoft.directionautoapp.model.MessageDataList;
import com.vallsoft.directionautoapp.model.api.PhoneCallResponse;
import com.vallsoft.directionautoapp.repository.MessageDataRepository;
import com.vallsoft.directionautoapp.repository.UserDataRepository;
import com.vallsoft.directionautoapp.service.VibrateService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Permission;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;
import gun0912.tedbottompicker.TedBottomSheetDialogFragment;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.RECORD_AUDIO;
import static com.vallsoft.directionautoapp.App.CHANNEL_1_ID;

public class MessageActivity extends AppCompatActivity implements MessageDataCallBack, BottomMediaDialog.BottomSheetListener {
    public static final String TAG = "MessageActivityLog";

    private static final int FROM_GALLERY_IMAGE = 12;
    private static final int FROM_FILE_SYSTEM = 13;
    private static final int CAMERA_REQUEST = 15;

    private static final int MY_READ_EXTERNAL_STORAGE_IMAGE_PERMISSIONS_CODE = 21;
    private static final int MY_READ_EXTERNAL_STORAGE_FILE_PERMISSIONS_CODE = 22;
    private static final int MY_CAMERA_PERMISSION_CODE = 23;
    private static final int REQUEST_AUDIO_PERMISSION_CODE = 24;
    private static final int MY_WRITE_EXTERNAL_STORAGE_IMAGE_PERMISSIONS_CODE = 25;
    private static final int MY_WRITE_EXTERNAL_STORAGE_SEND_MESSAGE_PERMISSIONS_CODE = 26;
    private static final int MY_WRITE_EXTERNAL_STORAGE_RECORD_AUDIO_PERMISSIONS_CODE = 27;
    private static final int MY_READ_EXTERNAL_STORAGE_IMAGE_FROM_CAMERA_PERMISSIONS_CODE = 28;

    private static String audioFilePath = null;
    private static String audioFilePathSDCard = null;

    private UserDataRepository userDataRepository;
    private MessageDataRepository messageDataRepository;

    private RecyclerView messageDataRecyclerView;
    private Chronometer recordVoiceChronometer;
    private ImageView recordVoiceImage;
    private TextView recordVoiceText;
    private MediaRecorder mediaRecorder = null;
    private MessageDataAdapter messageDataAdapter;
    private Button sendButton;
    private EditText inputDescriptionEditText;

    private List<MessageData> messageDataList;
    private boolean isRecordingVoice = false;
    private long recordVoiceTime = 0;

    private String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private String dataPath = SDPath + "/direct_auto_app/data/";
    private String zipPathFolder = SDPath + "/direct_auto_app/zip/";
    private String zipName = "messageFiles.zip";

    private ArrayList<Uri> selectedUriList = new ArrayList<>();

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        EditText editor = new EditText(this);
        editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        messageDataRepository = new MessageDataRepository(this);
        userDataRepository = new UserDataRepository(this);

        recordVoiceChronometer = findViewById(R.id.record_voice_chronometer);
        recordVoiceImage = findViewById(R.id.record_voice_image);
        recordVoiceText = findViewById(R.id.record_voice_text);
        LinearLayout makePhotoVideoButton = findViewById(R.id.make_photo_video_button);
        LinearLayout selectFromGalleryButton = findViewById(R.id.select_from_gallery_button);
        LinearLayout selectFromFilesystemButton = findViewById(R.id.select_from_filesystem_button);
        LinearLayout recordVoiceButton = findViewById(R.id.record_voice_button);
        sendButton = findViewById(R.id.send_button);
        inputDescriptionEditText = findViewById(R.id.input_description);
        inputDescriptionEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        if (!messageDataRepository.getDescription().equals(""))
            inputDescriptionEditText.setText(messageDataRepository.getDescription());

        if (messageDataRepository.getMessageDataList() == null)
            messageDataList = new ArrayList<>();
        else
            messageDataList = messageDataRepository.getMessageDataList().getMessageDataList();
        setClickedMainButton();
        messageDataRecyclerView = findViewById(R.id.message_data_list);
        messageDataAdapter = new MessageDataAdapter(this, messageDataList, this);
        messageDataRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        messageDataRecyclerView.setAdapter(messageDataAdapter);

        inputDescriptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                setClickedMainButton();
            }
        });

        findViewById(R.id.message_content_container).setOnClickListener(view -> hideKeyBoard());

        findViewById(R.id.message_activity_container).setOnClickListener(view -> hideKeyBoard());

        findViewById(R.id.close_activity).setOnClickListener(view -> onBackPressed());

        sendButton.setOnClickListener(view -> {
            if (messageDataList.size() == 0) {
                sendCallMeBackWithText();
            } else {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_WRITE_EXTERNAL_STORAGE_SEND_MESSAGE_PERMISSIONS_CODE);
                } else {
                    sendCallMeBackWithFile();
                }
            }
        });

        makePhotoVideoButton.setOnClickListener(view -> makePhotoButtonClick());

        selectFromGalleryButton.setOnClickListener(view -> {
            hideKeyBoard();
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_READ_EXTERNAL_STORAGE_IMAGE_PERMISSIONS_CODE);
            } else {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_WRITE_EXTERNAL_STORAGE_IMAGE_PERMISSIONS_CODE);
                } else {
//                    BottomMediaDialog dialog = new BottomMediaDialog();
//                    dialog.show(getSupportFragmentManager(), "bottomMediaDialog");
//                    PermissionListener permissionListener = new PermissionListener() {
//                        @Override
//                        public void onPermissionGranted() {
//                            openBottomPicker();
//                        }
//
//                        @Override
//                        public void onPermissionDenied(List<String> deniedPermissions) {
//                            Toast.makeText(MessageActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
//                        }
//                    };
//
//                    TedPermission.with(MessageActivity.this)
//                            .setPermissionListener(permissionListener)
//                            .setDeniedMessage("If you reject permission, you can no use this service\nPlease turn on permissions")
//                            .setPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE)
//                            .check();

                    // Use the GET_CONTENT intent from the utility class
                    Intent intent = FileUtils.createGetContentIntent(ChooserType.MEDIA_FILE);
                    startActivityForResult(intent, FROM_GALLERY_IMAGE);

//                    Intent intent= new Intent(this, Gallery.class);
//                    // Set the title
//                    intent.putExtra("title","Select media");
//                    // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
//                    intent.putExtra("mode",1);
//                    intent.putExtra("maxSelection",100); // Optional
//                    startActivityForResult(intent, FROM_GALLERY_IMAGE);

//                    Intent intent = new Intent(MessageActivity.this, CustomGalleryActivity.class);
//                    startActivityForResult(intent, FROM_GALLERY_IMAGE);
                }
            }
        });

        selectFromFilesystemButton.setOnClickListener(view -> {
            hideKeyBoard();
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_READ_EXTERNAL_STORAGE_FILE_PERMISSIONS_CODE);
            } else {
                // Use the GET_CONTENT intent from the utility class
                Intent target = FileUtils.createGetContentIntent(ChooserType.FILE);
                // Create the chooser Intent
                Intent intent = Intent.createChooser(target, getString(R.string.chooser_title));
                try {
                    startActivityForResult(intent, FROM_FILE_SYSTEM);
                } catch (ActivityNotFoundException e) {
                    // The reason for the existence of aFileChooser
                    Log.e(TAG, e.toString());
                }
            }
        });

        recordVoiceButton.setOnClickListener(view -> recordVoiceButtonClick());
    }

    private void openBottomPicker() {
//        TedBottomPicker.OnMultiImageSelectedListener listener = new TedBottomPicker.OnMultiImageSelectedListener() {
//            @Override
//            public void onImagesSelected(ArrayList<Uri> uriList) {
//                for(Uri uri: uriList) {
//                    Log.d("111111", uri.toString());
//                    readImageMetaData(uri);
//                }
//            }
//        };
//
//        TedBottomPicker tedBottomPicker = new TedBottomPicker.Builder(MessageActivity.this)
//                .showVideoMedia()
//                .setOnMultiImageSelectedListener(listener)
//                .setCompleteButtonText("Done")
//                .setEmptySelectionText("No item")
//                .create();
//        tedBottomPicker.show(getSupportFragmentManager());

//        List<Uri> selectedUriList = new ArrayList<>();
//        TedBottomPicker.with(MessageActivity.this)
//                .showTitle(false)
//                .setCompleteButtonText("Done")
//                .setEmptySelectionText("No Select")
//                .setSelectedUriList(selectedUriList)
//                .showVideoMedia()
//                .showMultiImage(new TedBottomSheetDialogFragment.OnMultiImageSelectedListener() {
//                    @Override
//                    public void onImagesSelected(List<Uri> uriList) {
//                        for(Uri uri: uriList) {
//                            readImageMetaData(uri);
//                        }
//                    }
//                });
    }

    @Override
    public void onPhotoClick() {
        TedBottomPicker.with(MessageActivity.this)
                .showTitle(false)
                .setCompleteButtonText("Done")
                .setEmptySelectionText("No Select")
                .setSelectedUriList(selectedUriList)
                .showMultiImage(new TedBottomSheetDialogFragment.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(List<Uri> uriList) {
                        for(Uri uri: uriList) {
                            readImageMetaData(uri);
                        }
                    }
                });
    }

    @Override
    public void onVideoClick() {
        TedBottomPicker.with(MessageActivity.this)
                .showTitle(false)
                .setCompleteButtonText("Done")
                .setEmptySelectionText("No Select")
                .setSelectedUriList(selectedUriList)
                .showVideoMedia()
                .showMultiImage(new TedBottomSheetDialogFragment.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(List<Uri> uriList) {
                        for(Uri uri: uriList) {
                            readImageMetaData(uri);
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (messageDataList.size() == 0 && inputDescriptionEditText.getText().toString().equals("")) {
            messageDataRepository.setDescription(inputDescriptionEditText.getText().toString());
            messageDataRepository.setMessageDataList(new MessageDataList(messageDataList));
            hideKeyBoard();
            super.onBackPressed();
            return;
        }
        confirmSavingChangesDialog();
    }

    public void saveMessageDataChanges() {
        messageDataRepository.setDescription(inputDescriptionEditText.getText().toString());
        messageDataRepository.setMessageDataList(new MessageDataList(messageDataList));
        if (!inputDescriptionEditText.getText().toString().equals(""))
            messageDataRepository.setDescription(inputDescriptionEditText.getText().toString());
        if (messageDataList.size() > 0) {
            MessageDataList messageDataListInstance = new MessageDataList(messageDataList);
            messageDataRepository.setMessageDataList(messageDataListInstance);
        }
        super.onBackPressed();
    }

    public void confirmSavingChangesDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.SAVE_CHANGES_TEXT));
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.NO), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                hideKeyBoard();
                MessageActivity.super.onBackPressed();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.YES), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                hideKeyBoard();
                saveMessageDataChanges();
            }
        });
        alertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void makePhotoButtonClick() {
        hideKeyBoard();
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_WRITE_EXTERNAL_STORAGE_IMAGE_PERMISSIONS_CODE);
        }
        else {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_READ_EXTERNAL_STORAGE_IMAGE_FROM_CAMERA_PERMISSIONS_CODE);
            }
            else {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Intent chooserIntent = Intent.createChooser(takePictureIntent, "Capture Image or Video");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takeVideoIntent});
                    startActivityForResult(chooserIntent, CAMERA_REQUEST);
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void recordVoiceButtonClick() {
        hideKeyBoard();
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_WRITE_EXTERNAL_STORAGE_RECORD_AUDIO_PERMISSIONS_CODE);
        }
        else {
            if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{RECORD_AUDIO}, REQUEST_AUDIO_PERMISSION_CODE);
            }
            else {
                if (!isRecordingVoice)
                    recordAudio();
                else
                    stopRecordAudio();
            }
        }
    }

    private void sendCallMeBackWithFile() {
        hideKeyBoard();
        ZipArchiver.deleteFileFromDataFolder();
        for (MessageData messageData : messageDataList) {
            ZipArchiver.saveToFile(dataPath, messageData);
        }
        if (ZipArchiver.zip(dataPath, zipPathFolder, zipName, false)) {
            File zipFile = new File(zipPathFolder + zipName);

            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"),
                    zipFile);
            MultipartBody.Part file = MultipartBody.Part.createFormData("file",
                    zipFile.getName(), requestFile);

            clearMessageDataList();

            final ProgressDialog progressDialog = new ProgressDialog(MessageActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.LOADING_TEXT));
            progressDialog.setCancelable(false);
            progressDialog.show();
            NetworkService.getInstance().getPhoneCallApi()
                    .callMeBackWithFile("380" + userDataRepository.getPhoneNumber(), getDescription(), file)
                    .enqueue(new Callback<PhoneCallResponse>() {
                        @Override
                        public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                            if (response.isSuccessful()) {
                                showNotificationCallAccepted();
                                inputDescriptionEditText.setText("");
                                startVibration();
                                Log.d(MessageActivity.TAG, "Success: " + response.body().getResult());
                            } else {
                                Log.d(MessageActivity.TAG, "Error: " + response.errorBody().toString());
                            }
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                            Log.e(MessageActivity.TAG, t.getMessage());
                            progressDialog.dismiss();
                        }
                    });
        }
    }

    private void sendCallMeBackWithText() {
        hideKeyBoard();
        clearMessageDataList();

        final ProgressDialog progressDialog = new ProgressDialog(MessageActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.LOADING_TEXT));
        progressDialog.setCancelable(false);
        progressDialog.show();
        NetworkService.getInstance().getPhoneCallApi()
                .callMeBackWithFile("380" + userDataRepository.getPhoneNumber(), getDescription())
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if (response.isSuccessful()) {
                            showNotificationCallAccepted();
                            inputDescriptionEditText.setText("");
                            startVibration();
                            Log.d(MessageActivity.TAG, "Success: " + response.body().getResult());
                        } else {
                            Log.d(MessageActivity.TAG, "Error: " + response.errorBody().toString());
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        Log.e(MessageActivity.TAG, t.getMessage());
                        progressDialog.dismiss();
                    }
                });
    }

    private String getDescription() {
        return inputDescriptionEditText.getText().toString();
    }

    private void hideKeyBoard() {
        inputDescriptionEditText.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inputDescriptionEditText.getWindowToken(), 0);
    }

    public void showNotificationCallAccepted() {
        Intent intent = new Intent(this, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 1,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new  NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_logo_small)
                .setColor(getResources().getColor(R.color.colorBlue))
                .setContentTitle(getString(R.string.CALL_ACCEPTED_TITLE))
                .setContentText(getString(R.string.CALL_ACCEPTED_BODY))
                .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(1, notification);
    }

    public void startVibration() {
        Intent intentVibrate = new Intent(getApplicationContext(), VibrateService.class);
        startService(intentVibrate);
    }

    @SuppressLint("SdCardPath")
    public void recordAudio() {
        recordVoiceChronometer.setVisibility(View.VISIBLE);
        recordVoiceChronometer.setBase(SystemClock.elapsedRealtime() + recordVoiceTime);
        recordVoiceChronometer.start();

        recordVoiceText.setText(getString(R.string.STOP));

        recordVoiceImage.setImageResource(R.drawable.ic_stop_record_voice);
        ((LinearLayout.LayoutParams) recordVoiceImage.getLayoutParams()).bottomMargin
                = (int) (getApplicationContext().getResources().getDisplayMetrics().density * 10);

        isRecordingVoice = true;

        audioFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        long currentTimeMillis = System.currentTimeMillis();
        audioFilePath += "/"+currentTimeMillis+".3gp";
        audioFilePathSDCard = "file:///sdcard/"+currentTimeMillis+".3gp";

        try {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mediaRecorder.setOutputFile(audioFilePath);
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IOException e) {
            Log.e(MessageActivity.TAG, e.getMessage());
        }
    }

    public void stopRecordAudio() {
        recordVoiceTime = recordVoiceChronometer.getBase() - SystemClock.elapsedRealtime();
        recordVoiceChronometer.stop();
        recordVoiceChronometer.setBase(SystemClock.elapsedRealtime());    // зануляє хронометр
        recordVoiceChronometer.setVisibility(View.GONE);
        recordVoiceTime = 0;

        recordVoiceText.setText(getString(R.string.AUDIO_RECORDER_BUTTON_LABEL));

        recordVoiceImage.setImageResource(R.drawable.ic_record_voice);
        ((LinearLayout.LayoutParams) recordVoiceImage.getLayoutParams()).bottomMargin
                = (int) (getApplicationContext().getResources().getDisplayMetrics().density * 15);

        isRecordingVoice = false;

        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;

        MessageData messageData = new MessageData(new File(audioFilePath), audioFilePath, FileType.AUDIO);
        addFileToList(messageData);
    }

    private void addFileToList(MessageData messageData) {
        Log.d(TAG, messageData.toString());

        messageDataList.add(messageData);
        messageDataAdapter.setDataList(messageDataList);
        messageDataAdapter.notifyItemInserted(messageDataList.size() - 1);
        sendButton.setAnimation(AnimationUtils.loadAnimation(this, R.anim.button_translate_down));

        setClickedMainButton();
    }

    @Override
    public void onRemoved(int position) {
        messageDataList.remove(position);
        messageDataAdapter.setDataList(messageDataList);
        messageDataAdapter.notifyItemRemoved(position);
        sendButton.setAnimation(AnimationUtils.loadAnimation(MessageActivity.this, R.anim.button_translate_up));

        setClickedMainButton();
    }

    private void clearMessageDataList() {
        messageDataList.clear();
        messageDataAdapter.setDataList(messageDataList);
        messageDataRecyclerView.setAdapter(messageDataAdapter);

        setClickedMainButton();
    }

    private void setClickedMainButton() {
        if (messageDataList.size() == 0 && inputDescriptionEditText.getText().toString().length() == 0) {
            sendButton.setBackgroundResource(R.drawable.background_button_disable);
            sendButton.setEnabled(false);
            return;
        }
        sendButton.setBackgroundResource(R.drawable.background_button);
        sendButton.setEnabled(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void makePhoto(Intent intent) {
        try {
            readImageMetaData(intent.getData());
        } catch (RuntimeException e) {
            Bitmap photoBitmap = (Bitmap) intent.getExtras().get("data");

            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();

            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");
            File photoFile = new File(extStorageDirectory, "IMG_"+ format.format(new Date()) + ".jpg");
            try {
                OutputStream outStream = new FileOutputStream(photoFile);
                photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();

                MessageData messageData;
                if (photoFile.getAbsolutePath().endsWith("mp4"))
                    messageData = new MessageData(photoFile, FileType.VIDEO);
                else
                    messageData = new MessageData(photoFile, FileType.IMAGE);
                addFileToList(messageData);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void readFileMetaData(Uri uri) {
        Log.d(TAG, "Uri: " + uri.toString());
        try {
            // Get the file path from the URI
            String filePath = FileUtils.getPath(this, uri);
            File file = new File(filePath);
            MessageData messageData = new MessageData(file, FileType.FILE);
            addFileToList(messageData);
        } catch (Exception e) {
            Log.e(TAG, "File select error", e);
        }
    }

    private void readImageMetaData(Uri uri) {
//        File file = new File(uri.getPath());
//        MessageData messageData;
//        if (uri.getPath().endsWith("mp4"))
//            messageData = new MessageData(file, FileType.VIDEO);
//        else
//            messageData = new MessageData(file, FileType.IMAGE);
//        addFileToList(messageData);

        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File file = new File(picturePath);
            MessageData messageData;
            if (picturePath.endsWith("mp4"))
                messageData = new MessageData(file, FileType.VIDEO);
            else
                messageData = new MessageData(file, FileType.IMAGE);
            addFileToList(messageData);
        }
    }

    private void readImageMetaData(String path) {
        File file = new File(path);
        MessageData messageData;
        if (path.endsWith("mp4"))
            messageData = new MessageData(file, FileType.VIDEO);
        else
            messageData = new MessageData(file, FileType.IMAGE);
        addFileToList(messageData);

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent == null)
            return;
        if (resultCode != RESULT_OK) {
            return;
        }

//        if (requestCode == FROM_GALLERY_IMAGE) {
//            ArrayList<String> selectionResult = intent.getStringArrayListExtra("result");
//            for (String path: selectionResult) {
//                readImageMetaData(path);
//            }
//        }

        if (intent.getClipData() == null) {
            Uri selectedFileUri = intent.getData();
            switch (requestCode) {
                case FROM_GALLERY_IMAGE:
                    readImageMetaData(selectedFileUri);
                    break;
                case FROM_FILE_SYSTEM:
                    readFileMetaData(selectedFileUri);
                    break;
                case CAMERA_REQUEST:
                    makePhoto(intent);
            }
        } else {
            int count = intent.getClipData().getItemCount();
            int currentItem = 0;
            while(currentItem < count) {
                Uri selectedFileUri = intent.getClipData().getItemAt(currentItem).getUri();
                switch (requestCode) {
                    case FROM_GALLERY_IMAGE:
                        readImageMetaData(selectedFileUri);
                        break;
                    case FROM_FILE_SYSTEM:
                        readFileMetaData(selectedFileUri);
                        break;
                    case CAMERA_REQUEST:
                        makePhoto(intent);
                }
                currentItem = currentItem + 1;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        switch (requestCode) {
            case MY_CAMERA_PERMISSION_CODE:
            case MY_READ_EXTERNAL_STORAGE_IMAGE_FROM_CAMERA_PERMISSIONS_CODE:
            case MY_WRITE_EXTERNAL_STORAGE_IMAGE_PERMISSIONS_CODE:
                makePhotoButtonClick();
                break;
            case MY_WRITE_EXTERNAL_STORAGE_RECORD_AUDIO_PERMISSIONS_CODE:
                recordVoiceButtonClick();
                break;
            case MY_WRITE_EXTERNAL_STORAGE_SEND_MESSAGE_PERMISSIONS_CODE:
                sendCallMeBackWithFile();
                break;
            case MY_READ_EXTERNAL_STORAGE_IMAGE_PERMISSIONS_CODE:
                // Use the GET_CONTENT intent from the utility class
                Intent imageIntent = FileUtils.createGetContentIntent(ChooserType.MEDIA_FILE);
                startActivityForResult(imageIntent, FROM_GALLERY_IMAGE);
                break;
            case MY_READ_EXTERNAL_STORAGE_FILE_PERMISSIONS_CODE:
                // Use the GET_CONTENT intent from the utility class
                Intent target = FileUtils.createGetContentIntent(ChooserType.FILE);
                // Create the chooser Intent
                Intent fileIntent = Intent.createChooser(target, getString(R.string.chooser_title));
                try {
                    startActivityForResult(fileIntent, FROM_FILE_SYSTEM);
                } catch (ActivityNotFoundException e) {
                    // The reason for the existence of aFileChooser
                    Log.e(TAG, e.toString());
                }
                break;
            case REQUEST_AUDIO_PERMISSION_CODE:
                recordAudio();
                break;
            }
    }
}