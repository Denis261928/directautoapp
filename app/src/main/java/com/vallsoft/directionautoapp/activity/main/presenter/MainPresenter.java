package com.vallsoft.directionautoapp.activity.main.presenter;

import android.content.Context;
import android.util.Log;

import com.vallsoft.directionautoapp.activity.main.MainActivity;
import com.vallsoft.directionautoapp.api.NetworkService;
import com.vallsoft.directionautoapp.helper.RequestMainPageStatus;
import com.vallsoft.directionautoapp.activity.main.view.MainView;
import com.vallsoft.directionautoapp.utils.NetworkUtils;
import com.vallsoft.directionautoapp.model.api.NotificationsCount;
import com.vallsoft.directionautoapp.model.api.PhoneCallResponse;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter {
    private MainView view;
    private UserDataRepository repository;

    public MainPresenter(MainView view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    public void makeMessageAsRead(String messageDate) {
        NetworkService.getInstance().getPhoneCallApi()
                .markMessageAsRead("380"+repository.getPhoneNumber(), messageDate)
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if (response.isSuccessful()) {
                            Log.d(MainActivity.TAG, "Result: " + response.body().getResult());
                        } else {
                            Log.d(MainActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        Log.d(MainActivity.TAG, t.getMessage());
                    }
                });
    }

    public void getCountOfNewMessages() {
        view.showProgress();
        NetworkService.getInstance().getPhoneCallApi().getCountOfNewMessages("380" + repository.getPhoneNumber())
                .enqueue(new Callback<NotificationsCount>() {
                    @Override
                    public void onResponse(Call<NotificationsCount> call, Response<NotificationsCount> response) {
                        view.hideProgress();
                        if (response.isSuccessful()) {
                            NotificationsCount notificationsCount = response.body();
                            Log.d(MainActivity.TAG, "Result: " + notificationsCount.getResult());
                            Log.d(MainActivity.TAG, "Count: " + notificationsCount.getCount());
                            if (notificationsCount.getResult().equals("OK")) {
                                if (notificationsCount.getCount() == 0)
                                    view.hideNotificationsCount();
                                else
                                    view.showNotificationsCount(notificationsCount.getCount());
                            }
                        } else {
                            Log.d(MainActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationsCount> call, Throwable t) {
                        view.hideProgress();
                        Log.d(MainActivity.TAG, t.getMessage());
                    }
                });
    }

    public void makeCall(Context context) {
        if (!NetworkUtils.getNetworkConnection(context)) {
            view.setScreenStatusBackground(RequestMainPageStatus.NO_INTERNET);
            view.setNoInternetVisible(true);
            return;
        }
        view.setScreenStatusBackground(RequestMainPageStatus.PHONE_CALL_MADE);
        NetworkService.getInstance().getPhoneCallApi()
                .callMeBack("380"+ repository.getPhoneNumber())
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if(response.isSuccessful()) {
                            view.showNotificationCallAccepted();
                            view.startVibration();
                            Log.d(MainActivity.TAG, "Success: "+ response.body().getResult());
                        } else {
                            Log.d(MainActivity.TAG, "Error: "+ response.errorBody().toString());
                        }
                        view.setScreenStatusBackground(RequestMainPageStatus.DEFAULT);
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        view.setScreenStatusBackground(RequestMainPageStatus.DEFAULT);
                        Log.e(MainActivity.TAG, t.getMessage());
                    }
                });

    }

    public void checkNetworkConnection(Context context) {
        if (NetworkUtils.getNetworkConnection(context)) {
            view.setScreenStatusBackground(RequestMainPageStatus.DEFAULT);
            view.setNoInternetVisible(false);
        } else {
            view.setScreenStatusBackground(RequestMainPageStatus.NO_INTERNET);
            view.setNoInternetVisible(true);
        }
    }

    public void checkAuthorized() {
        if (repository.getPhoneNumber() == null || repository.getPhoneNumber().equals("")
            || repository.getDeviceToken() == null || repository.getDeviceToken().equals("")) {
            view.startAuthActivity();
        }
//        if (repository.getPhoneNumber() == null || repository.getPhoneNumber().equals("")) {
//            view.startAuthActivity();
//        }
    }

}
