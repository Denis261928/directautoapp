package com.vallsoft.directionautoapp.activity.message.adapter;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.activity.message.MessageActivity;
import com.vallsoft.directionautoapp.helper.FileType;
import com.vallsoft.directionautoapp.model.MessageData;
import com.vallsoft.directionautoapp.activity.message.helper.MessageDataCallBack;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final static String TAG = MessageDataAdapter.class.getName();

    private static MediaPlayer mediaPlayer;
    private static int positionPlayed = 0;

    private MessageDataCallBack messageDataCallBack;

    private List<MessageData> dataList;
    private Context activityContext;

    public MessageDataAdapter(Context activityContext, List<MessageData> dataList, MessageDataCallBack messageDataCallBack) {
        mediaPlayer = new MediaPlayer();
        this.dataList = dataList;
        this.activityContext = activityContext;
        this.messageDataCallBack = messageDataCallBack;
    }

    public void setDataList(List<MessageData> dataList) {
        this.dataList = dataList;
    }

    @Override
    public int getItemViewType(int position) {
        return dataList.get(position).getFileType() == FileType.AUDIO ? 1 : 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RelativeLayout view;
        // 0 - file added
        if (viewType == 0) {
            view = (RelativeLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_file, parent, false);
            return new ViewHolderFile(view);
        }
        // 1 - audio added
        else  {
            view = (RelativeLayout) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_audio, parent, false);
            return new ViewHolderAudio(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == 0) {
            ViewHolderFile viewHolder = (ViewHolderFile) holder;
            viewHolder.setItem(dataList.get(position));
        } else {
            ViewHolderAudio viewHolder = (ViewHolderAudio) holder;
            viewHolder.setItem(dataList.get(position), position);
            viewHolder.setContext(activityContext);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private class ViewHolderFile extends RecyclerView.ViewHolder {
        private RelativeLayout itemContainer;
        private FrameLayout fileIconContainer;
        private ImageView imageView;
        private TextView nameText;

        public ViewHolderFile(@NonNull RelativeLayout itemView) {
            super(itemView);
            itemContainer = itemView;
            fileIconContainer = itemView.findViewById(R.id.file_frame_image_item);
            imageView = itemView.findViewById(R.id.file_image_item);
            imageView = itemView.findViewById(R.id.file_image_item);
            nameText = itemView.findViewById(R.id.file_name_item);

            itemView.findViewById(R.id.cancel_item).setOnClickListener(view -> {
                messageDataCallBack.onRemoved(getAdapterPosition());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        itemContainer.setAnimation(AnimationUtils.loadAnimation(activityContext, R.anim.item_fall_up));
                    }
                }, 100);
            });
        }


        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public void setItem(MessageData messageData) {
            itemContainer.setAnimation(AnimationUtils.loadAnimation(activityContext, R.anim.item_fall_down));

            this.nameText.setText(messageData.getFile().getName());

            if (messageData.getFileType() == FileType.IMAGE) {
                try {
                    this.imageView.setImageBitmap(BitmapFactory.decodeFile(messageData.getFile().getPath()));
                    fileIconContainer.setVisibility(View.GONE);
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class ViewHolderAudio extends RecyclerView.ViewHolder {
        private RelativeLayout itemContainer;
        private Context viewHolderAudioContext;
        private ImageView playPauseAudioButton;
        private TextView nameText;
        private TextView durationOfAudio;
        private ProgressBar audioProgressBar;

        @SuppressLint("SimpleDateFormat")
        private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        private MessageData messageData;
        private String currentAudioPath = null;

        private int position;
        private int totalTime = 0;
        private Handler handler = new Handler();

        private final AtomicBoolean threadRunning = new AtomicBoolean(false);
        Thread startProgress = new Thread(new Runnable() {
            @Override
            public void run() {
                while (threadRunning.get()) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (dataList.size() > 0) {
                                if (isCurrentAudioPlaying()) {
                                    if (mediaPlayer.getCurrentPosition() + 100 > mediaPlayer.getDuration()) {
                                        mediaPlayer.pause();
                                        mediaPlayer.seekTo(0);
                                        audioProgressBar.setProgress(0);
                                        durationOfAudio.setText(createTimeLabel(totalTime));
                                    }
                                    if (!mediaPlayer.isPlaying()) {
                                        playPauseAudioButton.setImageResource(R.drawable.ic_play);
                                    }
                                    audioProgressBar.setProgress(mediaPlayer.getCurrentPosition());
                                    String elapsedTime = createTimeLabel(mediaPlayer.getCurrentPosition());
                                    durationOfAudio.setText(elapsedTime);
                                }
                            }
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        public ViewHolderAudio(@NonNull RelativeLayout itemView) {
            super(itemView);
            itemContainer = itemView;
            playPauseAudioButton = itemView.findViewById(R.id.play_pause_audio_button);
            nameText = itemView.findViewById(R.id.file_name_item);
            durationOfAudio = itemView.findViewById(R.id.file_time_item);
            audioProgressBar = itemView.findViewById(R.id.audio_progress_bar);

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.seekTo(0);
                    audioProgressBar.setProgress(0);
                    durationOfAudio.setText(createTimeLabel(totalTime));
                    playPauseAudioButton.setImageResource(R.drawable.ic_play);
                }
            });

            itemView.findViewById(R.id.cancel_item).setOnClickListener(view -> {
                messageDataCallBack.onRemoved(getAdapterPosition());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        itemContainer.setAnimation(AnimationUtils.loadAnimation(activityContext, R.anim.item_fall_up));
                    }
                }, 100);
            });

            playPauseAudioButton.setOnClickListener(view -> {
                currentAudioPlaying();
                // Коли нажали на кнопку play вже повторно
                if (positionPlayed == position) {
                    // play
                    if (!mediaPlayer.isPlaying()) {
                        mediaPlayer.start();
                        playPauseAudioButton.setImageResource(R.drawable.ic_pause);
                        Log.d(MessageActivity.TAG, "Той самий запис запустити "+ mediaPlayer.getDuration());

                    }
                    // pause
                    else {
                        mediaPlayer.pause();
                        playPauseAudioButton.setImageResource(R.drawable.ic_play);
                        Log.d(MessageActivity.TAG, "Той самий запис на паузу "+ mediaPlayer.getDuration());

                    }
                }
                // Коли нажали на кнопку play вперше. попереднє audio зупинеться
                // і audio яке є в даному елементі ліста запишеться в mediaPlayer і почне грати
                else {
                    playNewAudio();
                }
            });
        }

        private void currentAudioPlaying() {
            for (int i = 0; i < dataList.size(); i++) {
                dataList.get(i).setPlaying(false);
            }
            dataList.get(this.getAdapterPosition()).setPlaying(true);
        }

        private boolean isCurrentAudioPlaying() {
            try {
                return dataList.get(this.getAdapterPosition()).isPlaying();
            } catch (IndexOutOfBoundsException e) {
                return false;
            }
        }

        private void playNewAudio() {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                Log.d(MessageActivity.TAG, "Старий запис на паузу"+ mediaPlayer.getDuration());
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
            }

            Log.d(TAG, currentAudioPath);

            mediaPlayer = MediaPlayer.create(viewHolderAudioContext, Uri.fromFile(messageData.getFile()));

            mediaPlayer.start();
            positionPlayed = position;
            playPauseAudioButton.setImageResource(R.drawable.ic_pause);

            Log.d(MessageActivity.TAG, "Новий запис запустити "+mediaPlayer.getDuration());

        }

        private String createTimeLabel(int time) {
            String timeLabel = "";
            int min = time / 1000 / 60;
            int sec = time / 1000 % 60;
            timeLabel = min +":";
            if (sec < 10) {
                timeLabel += "0";
            }
            timeLabel += sec;

            return timeLabel;
        }

        public void setItem(MessageData messageData, final int position) {
            itemContainer.setAnimation(AnimationUtils.loadAnimation(activityContext, R.anim.item_fall_down));

            this.messageData = messageData;
            this.currentAudioPath = messageData.getAudioPath();
            this.position = position;
            this.playPauseAudioButton.setImageResource(R.drawable.ic_play);
            this.nameText.setText(dateFormat.format(new Date()));

            threadRunning.set(true);
            try {
                startProgress.start();
            } catch (Exception ignored){ }
            onCreateMediaPlayer();
        }

        public void setContext(Context context) {
            this.viewHolderAudioContext = context;
        }

        private void onCreateMediaPlayer(){
            mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource(currentAudioPath);
                positionPlayed = position;
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mediaPlayer.seekTo(0);
            mediaPlayer.setVolume(1f, 1f);
            totalTime = mediaPlayer.getDuration();
            durationOfAudio.setText(createTimeLabel(totalTime));
            audioProgressBar.setMax(totalTime);
        }
    }

}
