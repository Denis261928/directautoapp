package com.vallsoft.directionautoapp.activity.auth.view;

public interface SmsCodeView {
    String getCode();
    boolean isInputCodeNumber1Full();
    boolean isInputCodeNumber2Full();
    boolean isInputCodeNumber3Full();
    boolean isInputCodeNumber4Full();
    boolean isInputCodeNumber5Full();
    boolean isInputCodeNumber6Full();
    void clearAllInputs();
    void navigateMainActivity();
    void showErrorDialog();
    void showProgress();
    void hideProgress();
}
