package com.vallsoft.directionautoapp.activity.auth.view;

public interface AuthView {
    String getPhoneNumber();
    void setPhoneNumber(String phoneNumber);
    void setEnabledCodeButton();
    void setDisabledCodeButton();
    void showSmsFragment();
    void hideSmsFragment();
    void showErrorDialog();
    void startMainActivity();
    void showProgress();
    void hideProgress();
}
