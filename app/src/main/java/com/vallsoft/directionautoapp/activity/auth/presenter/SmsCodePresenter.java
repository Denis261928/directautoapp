package com.vallsoft.directionautoapp.activity.auth.presenter;

import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.vallsoft.directionautoapp.activity.auth.AuthActivity;
import com.vallsoft.directionautoapp.activity.auth.view.SmsCodeView;
import com.vallsoft.directionautoapp.api.NetworkService;
import com.vallsoft.directionautoapp.model.api.PhoneCallResponse;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SmsCodePresenter {
    public boolean isRequestSent = false;

    private String deviceToken;
    private SmsCodeView view;
    private UserDataRepository repository;

    public SmsCodePresenter(SmsCodeView view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    public void checkFullInputs() {
        if (view.isInputCodeNumber1Full()
                && view.isInputCodeNumber2Full()
                && view.isInputCodeNumber3Full()
                && view.isInputCodeNumber4Full()
                && view.isInputCodeNumber5Full()
                && view.isInputCodeNumber6Full()
                && !isRequestSent) {
            authorize();
            isRequestSent = true;
        }
    }

    public void authorize() {
        if (!repository.getCode().equals(view.getCode())) {
            view.showErrorDialog();
            return;
        }
        view.showProgress();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                repository.setCode("");
                getTokenFromFirebase();
            }
        }, 500);
    }

    private void getTokenFromFirebase() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (task.isSuccessful()) {
                    String token = task.getResult().getToken();
                    Log.d(AuthActivity.TAG, "onComplete: Token: " + token);
                    deviceToken = token;
                    repository.setDeviceToken(deviceToken);
                    setToken(deviceToken);
                } else {
                    Log.d(AuthActivity.TAG, "Token generation failed");
                    view.hideProgress();
                }
            }
        });
    }

    private void setToken(final String token) {
        NetworkService.getInstance().getPhoneCallApi().setToken("380"+repository.getPhoneNumber(), token)
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if (response.isSuccessful()) {
                            Log.d(AuthActivity.TAG, "Success: "+response.body().toString());
                            view.clearAllInputs();
                            view.navigateMainActivity();
                        }
                        else {
                            Log.d(AuthActivity.TAG, "Error: "+response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        view.hideProgress();
                        Log.d(AuthActivity.TAG, t.getMessage());
                    }
                });
    }

}
