package com.vallsoft.directionautoapp.activity.profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.activity.notification.NotificationActivity;
import com.vallsoft.directionautoapp.activity.profile.presenter.ProfilePresenter;
import com.vallsoft.directionautoapp.activity.profile.view.ProfileView;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

public class ProfileActivity extends AppCompatActivity implements ProfileView {
    public static final String TAG = ProfileActivity.class.getName();

    private RelativeLayout notificationCountContainer;
    private TextView notificationCountText;
    private EditText inputPhoneNumber;
    private ProgressDialog progressDialog;

    private ProfilePresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        notificationCountContainer = findViewById(R.id.notification_count_container);
        notificationCountText = findViewById(R.id.notification_count_text);
        LinearLayout emptySpaceLayout = findViewById(R.id.empty_space_layout);
        inputPhoneNumber = findViewById(R.id.input_phone_number);
        ImageView enabledInputPhoneNumber = findViewById(R.id.edit_phone_number);
        TextView logoutButtonText = findViewById(R.id.logout_button_text);

        String logoutText = getString(R.string.LOGOUT_TEXT_LINK);
        SpannableString content = new SpannableString(logoutText);
        content.setSpan(new UnderlineSpan(), 0, logoutText.length(), 0);
        logoutButtonText.setText(content);

        final UserDataRepository repository = new UserDataRepository(this);
        presenter = new ProfilePresenter(this, repository);
        inputPhoneNumber.setText(presenter.getPhoneNumber());

        findViewById(R.id.close_activity).setOnClickListener(view -> {
            hideKeyBoard();
            onBackPressed();
        });

        findViewById(R.id.open_notification_button).setOnClickListener(view -> {
            hideKeyBoard();
            startNewActivity(NotificationActivity.class);
        });

        enabledInputPhoneNumber.setOnClickListener(view -> {
            inputPhoneNumber.setEnabled(true);
            inputPhoneNumber.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(inputPhoneNumber, 1);
        });

        emptySpaceLayout.setOnClickListener(view -> presenter.inputPhoneNumberChanged());

        inputPhoneNumber.setOnEditorActionListener((textView, i, keyEvent) -> {
            if(i == EditorInfo.IME_ACTION_DONE ) {
                presenter.inputPhoneNumberChanged();
                return true;
            }
            return false;
        });

        logoutButtonText.setOnClickListener(view -> presenter.logoutClick());
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getCountOfNewMessages();
    }

    @Override
    public void startNewActivity(Class<?> activityClass) {
        startActivity(new Intent(ProfileActivity.this, activityClass));
    }

    @Override
    public String getPhoneNumber() {
        return inputPhoneNumber.getText().toString().replace(" ", "");
    }

    @Override
    public void showNotificationsCount(int count) {
        notificationCountContainer.setVisibility(View.VISIBLE);
        notificationCountText.setText(String.valueOf(count));
    }

    @Override
    public void hideNotificationsCount() {
        notificationCountContainer.setVisibility(View.GONE);
    }

    @Override
    public void confirmSavingDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.CHANGING_NUMBER_ALERT_TITLE));
        alertDialog.setMessage(getString(R.string.CHANGING_NUMBER_ALERT_BODY));
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.NO), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                inputPhoneNumber.setText(presenter.getPhoneNumber());
                hideKeyBoard();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.YES), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                presenter.savePhoneNumber();
                hideKeyBoard();
            }
        });
        alertDialog.show();
    }

    @Override
    public void inputIsIncompleteDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.CHANGING_NUMBER_ERROR_ALERT_TITLE));
        alertDialog.setMessage(getString(R.string.CHANGING_NUMBER_ERROR_ALERT_BODY));
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.NO), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.YES), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                inputPhoneNumber.setText(presenter.getPhoneNumber());
                hideKeyBoard();
            }
        });
        alertDialog.show();
    }

    @Override
    public void hideKeyBoard() {
        inputPhoneNumber.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inputPhoneNumber.getWindowToken(), 0);
    }

    @Override
    public void setPhoneNumberText(String phoneNumber) {
        inputPhoneNumber.setText(phoneNumber);
    }

    @Override
    public void showLogoutConfirmAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.LOGOUT_ALERT_TITLE));
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.NO), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.YES), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                presenter.logout();
            }
        });
        alertDialog.show();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.LOADING_TEXT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}