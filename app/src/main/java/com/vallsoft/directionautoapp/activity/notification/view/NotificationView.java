package com.vallsoft.directionautoapp.activity.notification.view;

import android.content.Context;

import com.vallsoft.directionautoapp.model.api.NotificationData;
import com.vallsoft.directionautoapp.helper.RequestMainPageStatus;

import java.util.List;

public interface NotificationView {
    Context getContext();
    void showNotifications(List<NotificationData> list);
    void setScreenStatusBackground(RequestMainPageStatus aDefault);
    void showProgress();
    void hideProgress();
}
