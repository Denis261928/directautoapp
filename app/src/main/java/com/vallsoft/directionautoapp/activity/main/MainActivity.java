package com.vallsoft.directionautoapp.activity.main;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.activity.auth.AuthActivity;
import com.vallsoft.directionautoapp.activity.main.view.MainView;
import com.vallsoft.directionautoapp.activity.notification.NotificationActivity;
import com.vallsoft.directionautoapp.activity.message.MessageActivity;
import com.vallsoft.directionautoapp.activity.main.presenter.MainPresenter;
import com.vallsoft.directionautoapp.activity.profile.ProfileActivity;
import com.vallsoft.directionautoapp.dialog.question.QuestionDialog;
import com.vallsoft.directionautoapp.helper.RequestMainPageStatus;
import com.vallsoft.directionautoapp.model.NotificationMessage;
import com.vallsoft.directionautoapp.model.QuestionData;
import com.vallsoft.directionautoapp.repository.UserDataRepository;
import com.vallsoft.directionautoapp.service.VibrateService;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.vallsoft.directionautoapp.App.CHANNEL_1_ID;

public class MainActivity extends AppCompatActivity implements MainView {
    public static final String TAG = "MainActivityLog";

    private FlowingDrawer mDrawer;
    private RelativeLayout notificationCountContainer;
    private TextView notificationCountText;
    private LinearLayout carKeyLayoutImage, contentMainBlock;
    private RelativeLayout noInternetButton;
    private ProgressDialog progressDialog;
    private Handler statusHandler = new Handler();

    private UserDataRepository repository;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        @SuppressLint("MissingPermission")
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "some_test");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        FirebaseMessaging.getInstance().subscribeToTopic("DirectAutoGeneral")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Successful";
                        if (!task.isSuccessful()) {
                            msg = "Failed";
                        }
                        Log.d("FirebaseMessaging", msg);
                    }
                });

        repository = new UserDataRepository(this);
        presenter = new MainPresenter(this, repository);
        // repository.setPhoneNumber("966683580");
        presenter.checkAuthorized();
        Log.d(TAG, "Device token: " + repository.getDeviceToken());
        Log.d(TAG, "Phone Number: " + repository.getPhoneNumber());

        NotificationMessage notificationMessage = (NotificationMessage) getIntent().getSerializableExtra("message");
        if (notificationMessage != null) {
            showMessageAlert(notificationMessage);
        }

        QuestionData questionData = (QuestionData) getIntent().getSerializableExtra("question");
        if (questionData != null) {
            QuestionDialog dialog = new QuestionDialog(this, repository, questionData, false);
            dialog.showDialog();
        }

        notificationCountContainer = findViewById(R.id.notification_count_container);
        notificationCountText = findViewById(R.id.notification_count_text);
        carKeyLayoutImage = findViewById(R.id.car_key_layout_image);
        contentMainBlock = findViewById(R.id.content_main_block);
        RelativeLayout makeCallButton = findViewById(R.id.call_main_button);
        RelativeLayout goToMessageButton = findViewById(R.id.message_main_button);
        noInternetButton = findViewById(R.id.no_internet_button);

        makeCallButton.setOnClickListener(view -> presenter.makeCall(MainActivity.this));

        goToMessageButton.setOnClickListener(view -> {
            setScreenStatusBackground(RequestMainPageStatus.MESSAGE_CLICKED);
            startActivity(new Intent(MainActivity.this, MessageActivity.class));
        });

        noInternetButton.setOnClickListener(view -> presenter.checkNetworkConnection(MainActivity.this));

        findViewById(R.id.open_notification_button).setOnClickListener(view -> startActivity(new Intent(MainActivity.this, NotificationActivity.class)));

        mDrawer = findViewById(R.id.drawerlayout);
        mDrawer.setTouchMode(ElasticDrawer.TOUCH_MODE_BEZEL);
        RelativeLayout mainLayout = findViewById(R.id.main_layout);
        ViewTreeObserver vto = mainLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    mainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    mainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                int width  = mainLayout.getMeasuredWidth();
                mDrawer.setMenuSize(width);
            }
        });

        findViewById(R.id.open_menu_button).setOnClickListener(view -> mDrawer.openMenu(true));

        findViewById(R.id.main_page_menu_item).setOnClickListener(view -> mDrawer.closeMenu(true));

        findViewById(R.id.profile_menu_item).setOnClickListener(view -> {
            mDrawer.closeMenu(true);
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.checkNetworkConnection(this);
        presenter.getCountOfNewMessages();
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isMenuVisible())
            mDrawer.closeMenu();
    }

    @Override
    public void setScreenStatusBackground(final RequestMainPageStatus status) {
        statusHandler.post(new Runnable() {
            @Override
            public void run() {
                switch (status) {
                    case DEFAULT:
                        carKeyLayoutImage.setBackgroundResource(R.drawable.main_page_default);
                        break;
                    case PHONE_CALL_MADE:
                        carKeyLayoutImage.setBackgroundResource(R.drawable.main_page_call_pressed);
                        break;
                    case MESSAGE_CLICKED:
                        carKeyLayoutImage.setBackgroundResource(R.drawable.main_page_message_pressed);
                        break;
                    case NO_INTERNET:
                        carKeyLayoutImage.setBackgroundResource(R.drawable.main_page_no_internet);
                        break;
                }
            }
        });
    }

    private void showMessageAlert(NotificationMessage notificationMessage) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(notificationMessage.getTitle());
        alertDialog.setMessage(notificationMessage.getMessage());
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.CLOSE), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
        presenter.makeMessageAsRead(notificationMessage.getMessageDate());
    }

    @Override
    public void setNoInternetVisible(boolean visibility) {
        if (visibility) {
            contentMainBlock.setVisibility(View.GONE);
            noInternetButton.setVisibility(View.VISIBLE);
            return;
        }
        contentMainBlock.setVisibility(View.VISIBLE);
        noInternetButton.setVisibility(View.GONE);
    }

    @Override
    public void showNotificationsCount(int count) {
        notificationCountContainer.setVisibility(View.VISIBLE);
        notificationCountText.setText(String.valueOf(count));
    }

    @Override
    public void hideNotificationsCount() {
        notificationCountContainer.setVisibility(View.GONE);
    }

    @Override
    public void startAuthActivity() {
        startActivity(new Intent(MainActivity.this, AuthActivity.class));
    }

    @Override
    public void showNotificationCallAccepted() {
        Intent intent = new Intent(this, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 1,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new  NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_logo_small)
                .setColor(getResources().getColor(R.color.colorBlue))
                .setContentTitle(getString(R.string.CALL_ACCEPTED_TITLE))
                .setContentText(getString(R.string.CALL_ACCEPTED_BODY))
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(1, notification);
    }

    @Override
    public void startVibration() {
        Intent intentVibrate = new Intent(getApplicationContext(), VibrateService.class);
        startService(intentVibrate);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.LOADING_TEXT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


}
