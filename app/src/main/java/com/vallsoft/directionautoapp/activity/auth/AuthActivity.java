package com.vallsoft.directionautoapp.activity.auth;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.activity.auth.fragment.SmsCodeFragment;
import com.vallsoft.directionautoapp.activity.auth.presenter.AuthPresenter;
import com.vallsoft.directionautoapp.activity.auth.view.AuthView;
import com.vallsoft.directionautoapp.activity.main.MainActivity;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

public class AuthActivity extends AppCompatActivity implements AuthView {
    public static final String TAG = "AuthActivityLog";

    private Button getCodeButton;
    private EditText inputPhoneNumberEditText;
    private TextView privatePolicyLink;
    private ProgressDialog progressDialog;
    private TextView sendSmsAgainLink;
    private Fragment fragment;

    private Handler showSmsCodeBlockHandler = new Handler();
    private UserDataRepository repository;
    private AuthPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        getCodeButton = findViewById(R.id.get_code_button);
        inputPhoneNumberEditText = findViewById(R.id.input_phone_number);
        privatePolicyLink = findViewById(R.id.privacy_policy_link);
        sendSmsAgainLink = findViewById(R.id.repeatedly_send_sms_link);

        repository = new UserDataRepository(this);
        presenter = new AuthPresenter(this, repository);
        // repository.setPhoneNumber("");
        presenter.checkAuthorized();
        presenter.setState();

        getCodeButton.setOnClickListener(view -> presenter.getCodeClicked());

        sendSmsAgainLink.setOnClickListener(view -> presenter.getCodeClicked());

        inputPhoneNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.inputPhoneNumberChanged(getCodeButton.isEnabled());
            }
        });

        privatePolicyLink.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://docs.google.com/document/d/1Px79Q2ub_KNjGPxcI084Rm_Qkz_Y4MM96fXIQQ63HvE/edit?usp=sharing"));
            startActivity(browserIntent);
        });
    }

    @Override
    public void onBackPressed() { }

    @Override
    public String getPhoneNumber() {
        return inputPhoneNumberEditText.getText().toString();
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        inputPhoneNumberEditText.setText(phoneNumber);
    }

    @Override
    public void setEnabledCodeButton() {
        getCodeButton.setEnabled(true);
        getCodeButton.setBackgroundResource(R.drawable.background_button);
    }

    @Override
    public void setDisabledCodeButton() {
        getCodeButton.setEnabled(false);
        getCodeButton.setBackgroundResource(R.drawable.background_button_disable);
    }

    @Override
    public void showSmsFragment() {
        showSmsCodeBlockHandler.post(new Runnable() {
            @Override
            public void run() {
                fragment = SmsCodeFragment.getNewInstance(presenter);
                getSupportFragmentManager().beginTransaction().add(R.id.input_sms_code_frame_fragment, fragment).commit();

                getCodeButton.setVisibility(View.GONE);
                privatePolicyLink.setVisibility(View.GONE);
                sendSmsAgainLink.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void hideSmsFragment() {
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();

        getCodeButton.setVisibility(View.VISIBLE);
        privatePolicyLink.setVisibility(View.VISIBLE);
        sendSmsAgainLink.setVisibility(View.GONE);
    }

    @Override
    public void showErrorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.ALERT_ERROR_TITLE));
        alertDialog.setMessage(getString(R.string.GETTING_CODE_ERROR));
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.CLOSE), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void clearPhoneNumberInput() {
        // Because after changing inputPhoneNumberEditText we clear phone number in repository.
        // But it is another case. We call this method when we before going to MainActivity(user is authorized)
        String phoneNumber = repository.getPhoneNumber();
        inputPhoneNumberEditText.setText("");
        repository.setPhoneNumber(phoneNumber);
    }

    @Override
    public void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(AuthActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.LOADING_TEXT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
