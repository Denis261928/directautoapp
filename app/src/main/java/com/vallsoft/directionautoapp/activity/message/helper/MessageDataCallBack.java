package com.vallsoft.directionautoapp.activity.message.helper;

public interface MessageDataCallBack {
    void onRemoved(int position);
}
