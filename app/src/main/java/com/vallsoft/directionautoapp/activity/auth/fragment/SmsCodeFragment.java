package com.vallsoft.directionautoapp.activity.auth.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.activity.auth.AuthActivity;
import com.vallsoft.directionautoapp.activity.auth.SmsReceiver;
import com.vallsoft.directionautoapp.activity.auth.presenter.AuthPresenter;
import com.vallsoft.directionautoapp.activity.auth.presenter.SmsCodePresenter;
import com.vallsoft.directionautoapp.activity.auth.view.SmsCodeView;
import com.vallsoft.directionautoapp.activity.main.MainActivity;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import java.util.ArrayList;
import java.util.List;

public class SmsCodeFragment extends Fragment implements SmsCodeView {
    private static final String AUTH_PRESENTER = "auth_presenter";

    private EditText inputCodeNumber1, inputCodeNumber2, inputCodeNumber3, inputCodeNumber4,
            inputCodeNumber5, inputCodeNumber6;
    private ProgressDialog progressDialog;

    private AuthPresenter authPresenter;
    private SmsCodePresenter presenter;

    private String pasteText = "";
    private boolean isPasteCode1 = false;
    private boolean isPasteCode2 = false;
    private boolean isPasteCode3 = false;
    private boolean isPasteCode4 = false;
    private boolean isPasteCode5 = false;
    private boolean isPasteCode6 = false;

    public static SmsCodeFragment getNewInstance(AuthPresenter authPresenter) {
        SmsCodeFragment smsCodeFragment = new SmsCodeFragment();
        Bundle args = new Bundle();
        args.putSerializable(AUTH_PRESENTER, authPresenter);
        smsCodeFragment.setArguments(args);

        return smsCodeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sms_code, null);

        UserDataRepository repository = new UserDataRepository(getActivity());
        presenter = new SmsCodePresenter(this, repository);

        inputCodeNumber1 = view.findViewById(R.id.input_code_number1);
        inputCodeNumber2 = view.findViewById(R.id.input_code_number2);
        inputCodeNumber3 = view.findViewById(R.id.input_code_number3);
        inputCodeNumber4 = view.findViewById(R.id.input_code_number4);
        inputCodeNumber5 = view.findViewById(R.id.input_code_number5);
        inputCodeNumber6 = view.findViewById(R.id.input_code_number6);
        inputCodeNumber1.requestFocus();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        authPresenter = (AuthPresenter) getArguments().getSerializable(AUTH_PRESENTER);
    }


    @Override
    public void onStart() {
        super.onStart();

        inputCodeNumber1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 > 2) {
                    isPasteCode1 = true;
                    pasteText = charSequence.toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPasteCode1) {
                            pasteText(pasteText);
                            isPasteCode1 = false;
                        }
                    }
                }, 1);

                if(isInputCodeNumber1Full()) {
                    inputCodeNumber2.requestFocus();
                    presenter.isRequestSent = false;
                    presenter.checkFullInputs();
                }
            }
        });

        inputCodeNumber2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 > 2) {
                    isPasteCode2 = true;
                    pasteText = charSequence.toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPasteCode2) {
                            pasteText(pasteText);
                            isPasteCode2 = false;
                        }
                    }
                }, 1);

                if(isInputCodeNumber2Full()) {
                    inputCodeNumber3.requestFocus();
                    presenter.isRequestSent = false;
                    presenter.checkFullInputs();
                } else {
                    inputCodeNumber1.requestFocus();
                }
            }
        });

        inputCodeNumber3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 > 2) {
                    isPasteCode3 = true;
                    pasteText = charSequence.toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPasteCode3) {
                            pasteText(pasteText);
                            isPasteCode3 = false;
                        }
                    }
                }, 1);

                if(isInputCodeNumber3Full()) {
                    inputCodeNumber4.requestFocus();
                    presenter.isRequestSent = false;
                    presenter.checkFullInputs();
                } else {
                    inputCodeNumber2.requestFocus();
                }
            }
        });

        inputCodeNumber4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 > 2) {
                    isPasteCode4 = true;
                    pasteText = charSequence.toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPasteCode4) {
                            pasteText(pasteText);
                            isPasteCode4 = false;
                        }
                    }
                }, 1);

                if(isInputCodeNumber4Full()) {
                    inputCodeNumber5.requestFocus();
                    presenter.isRequestSent = false;
                    presenter.checkFullInputs();
                } else {
                    inputCodeNumber3.requestFocus();
                }
            }
        });

        inputCodeNumber5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 > 2) {
                    isPasteCode5 = true;
                    pasteText = charSequence.toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPasteCode5) {
                            pasteText(pasteText);
                            isPasteCode5 = false;
                        }
                    }
                }, 1);

                if(isInputCodeNumber5Full()) {
                    inputCodeNumber6.requestFocus();
                    presenter.isRequestSent = false;
                    presenter.checkFullInputs();
                } else {
                    inputCodeNumber4.requestFocus();
                }
            }
        });

        inputCodeNumber6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 > 2) {
                    isPasteCode6 = true;
                    pasteText = charSequence.toString();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPasteCode6) {
                            pasteText(pasteText);
                            isPasteCode6 = false;
                        }
                    }
                }, 1);

                if(isInputCodeNumber6Full()) {
                    presenter.isRequestSent = false;
                    presenter.checkFullInputs();
                } else {
                    inputCodeNumber5.requestFocus();
                }
            }
        });

        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Android will provide message once receive. Start your broadcast receiver.
                IntentFilter filter = new IntentFilter();
                filter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
                getActivity().registerReceiver(new SmsReceiver(), filter);
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
            }
        });
    }

    private synchronized void pasteText(String code) {
        inputCodeNumber1.setText(code.substring(0, 1));
        inputCodeNumber2.setText(code.substring(1, 2));
        inputCodeNumber3.setText(code.substring(2, 3));
        inputCodeNumber4.setText(code.substring(3, 4));
        inputCodeNumber5.setText(code.substring(4, 5));
        inputCodeNumber6.setText(code.substring(5, 6));
    }

    @Override
    public String getCode() {
        return inputCodeNumber1.getText().toString() +
                inputCodeNumber2.getText().toString() +
                inputCodeNumber3.getText().toString() +
                inputCodeNumber4.getText().toString() +
                inputCodeNumber5.getText().toString() +
                inputCodeNumber6.getText().toString();
    }

    @Override
    public boolean isInputCodeNumber1Full() {
        return inputCodeNumber1.getText().toString().length() > 0;
    }

    @Override
    public boolean isInputCodeNumber2Full() {
        return inputCodeNumber2.getText().toString().length() > 0;
    }

    @Override
    public boolean isInputCodeNumber3Full() {
        return inputCodeNumber3.getText().toString().length() > 0;
    }

    @Override
    public boolean isInputCodeNumber4Full() {
        return inputCodeNumber4.getText().toString().length() > 0;
    }

    @Override
    public boolean isInputCodeNumber5Full() {
        return inputCodeNumber5.getText().toString().length() > 0;
    }

    @Override
    public boolean isInputCodeNumber6Full() {
        return inputCodeNumber6.getText().toString().length() > 0;
    }

    @Override
    public void clearAllInputs() {
        inputCodeNumber1.setText("");
        inputCodeNumber2.setText("");
        inputCodeNumber3.setText("");
        inputCodeNumber4.setText("");
        inputCodeNumber5.setText("");
        inputCodeNumber6.setText("");
        try {
            AuthActivity authActivity = (AuthActivity) getActivity();
            authActivity.clearPhoneNumberInput();
        } catch (NullPointerException e) {
            Log.e(AuthActivity.TAG, e.getStackTrace().toString());
        }
    }

    @Override
    public void navigateMainActivity() {
        authPresenter.checkAuthorized();
    }

    @Override
    public void showErrorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.ALERT_ERROR_TITLE));
        alertDialog.setMessage(getString(R.string.WRONG_CODE_ERROR));
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.CANCEL), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                inputCodeNumber1.setText("");
                inputCodeNumber2.setText("");
                inputCodeNumber3.setText("");
                inputCodeNumber4.setText("");
                inputCodeNumber5.setText("");
                inputCodeNumber6.setText("");
                inputCodeNumber1.requestFocus();
            }
        });
        alertDialog.show();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.LOADING_TEXT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
