package com.vallsoft.directionautoapp.activity.notification.adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.model.api.NotificationData;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
    private List<NotificationData> notificationsList;
    private OnNotificationListener onNotificationListener;

    public NotificationAdapter(List<NotificationData> notificationsList, OnNotificationListener onNotificationListener) {
        this.notificationsList = notificationsList;
        this.onNotificationListener = onNotificationListener;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);

        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        holder.setItem(notificationsList.get(position), onNotificationListener);
    }

    @Override
    public int getItemCount() {
        return notificationsList.size();
    }

    static class NotificationViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView descriptionTextView;
        private TextView dateTextView;

        private NotificationData notificationData;
        private OnNotificationListener listener;

        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.notification_name);
            descriptionTextView = itemView.findViewById(R.id.notification_description);
            dateTextView = itemView.findViewById(R.id.notification_date);

            itemView.setOnClickListener(view -> {
                listener.onNotificationClick(notificationData);
            });
        }

        public void setItem(NotificationData notificationData, OnNotificationListener listener) {
            this.notificationData = notificationData;
            this.listener = listener;
            nameTextView.setText(notificationData.getTheme());
            if (notificationData.getType().equals("poll"))
                descriptionTextView.setText(notificationData.getPollQuestion());
            else
                descriptionTextView.setText(notificationData.getText());

            if (!notificationData.isRead()) {
                nameTextView.setTypeface(null, Typeface.BOLD);
                descriptionTextView.setTypeface(null, Typeface.BOLD);
            }

            dateTextView.setText(notificationData.getDate());
        }
    }

    public interface OnNotificationListener {
        void onNotificationClick(NotificationData notificationData);
    }
}
