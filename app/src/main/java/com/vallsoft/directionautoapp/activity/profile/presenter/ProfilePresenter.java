package com.vallsoft.directionautoapp.activity.profile.presenter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.vallsoft.directionautoapp.activity.auth.AuthActivity;
import com.vallsoft.directionautoapp.activity.main.MainActivity;
import com.vallsoft.directionautoapp.activity.profile.ProfileActivity;
import com.vallsoft.directionautoapp.activity.profile.view.ProfileView;
import com.vallsoft.directionautoapp.api.NetworkService;
import com.vallsoft.directionautoapp.model.api.NotificationsCount;
import com.vallsoft.directionautoapp.model.api.PhoneCallResponse;
import com.vallsoft.directionautoapp.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter {
    private ProfileView view;
    private UserDataRepository repository;

    public ProfilePresenter(ProfileView view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    public String getPhoneNumber() {
        return repository.getPhoneNumber();
    }

    public void getCountOfNewMessages() {
        view.showProgress();
        NetworkService.getInstance().getPhoneCallApi().getCountOfNewMessages("380" + repository.getPhoneNumber())
                .enqueue(new Callback<NotificationsCount>() {
                    @Override
                    public void onResponse(Call<NotificationsCount> call, Response<NotificationsCount> response) {
                        view.hideProgress();
                        if (response.isSuccessful()) {
                            NotificationsCount notificationsCount = response.body();
                            Log.d(MainActivity.TAG, "Result: " + notificationsCount.getResult());
                            Log.d(MainActivity.TAG, "Count: " + notificationsCount.getCount());
                            if (notificationsCount.getResult().equals("OK")) {
                                if (notificationsCount.getCount() == 0)
                                    view.hideNotificationsCount();
                                else
                                    view.showNotificationsCount(notificationsCount.getCount());
                            }
                        } else {
                            Log.d(MainActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<NotificationsCount> call, Throwable t) {
                        view.hideProgress();
                        Log.d(MainActivity.TAG, t.getMessage());
                    }
                });
    }

    public void savePhoneNumber() {
        view.showProgress();
        Log.d("111111", view.getPhoneNumber());
        Log.d("111111", repository.getPhoneNumber());
        NetworkService.getInstance().getPhoneCallApi()
                .updatePhoneNumber("380"+repository.getPhoneNumber(), "380"+view.getPhoneNumber())
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("OK")) {
                                repository.setPhoneNumber(view.getPhoneNumber());
                                view.hideProgress();
                            }
                            else {
                                getTokenFromFirebase();
                            }
                            view.setPhoneNumberText(repository.getPhoneNumber());
                            Log.d(ProfileActivity.TAG+"Log", response.body().getResult());
                        }
                        else {
                            Log.d(ProfileActivity.TAG+"Log", response.errorBody().toString());
                            view.hideKeyBoard();
                        }
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        view.hideProgress();
                        Log.d(ProfileActivity.TAG+"Log", t.getMessage());
                    }
                });
    }

    private void getTokenFromFirebase() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (task.isSuccessful()) {
                    String token = task.getResult().getToken();
                    Log.d(AuthActivity.TAG, "onComplete: Token: " + token);
                    String deviceToken = token;
                    repository.setDeviceToken(deviceToken);
                    setToken(deviceToken);
                } else {
                    Log.d(ProfileActivity.TAG+"Log", "Token generation failed");
                }
            }
        });
    }

    private void setToken(final String token) {
        NetworkService.getInstance().getPhoneCallApi().setToken("380"+repository.getPhoneNumber(), token)
                .enqueue(new Callback<PhoneCallResponse>() {
                    @Override
                    public void onResponse(Call<PhoneCallResponse> call, Response<PhoneCallResponse> response) {
                        if (response.isSuccessful()) {
                            Log.d(ProfileActivity.TAG+"Log", "Success: "+response.body().toString());
                            savePhoneNumber();
                        }
                        else {
                            Log.d(ProfileActivity.TAG+"Log", "Error: "+response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<PhoneCallResponse> call, Throwable t) {
                        view.hideProgress();
                        Log.d(ProfileActivity.TAG+"Log", t.getMessage());
                    }
                });
    }

    private boolean isPhoneNumberFull() {
        return view.getPhoneNumber().length() == 9;
    }

    public void inputPhoneNumberChanged() {
        if (view.getPhoneNumber().equals(repository.getPhoneNumber())) {
            view.hideKeyBoard();
        }
        else {
            if (isPhoneNumberFull()) {
                view.confirmSavingDialog();
            } else {
                view.inputIsIncompleteDialog();
            }
        }
    }

    public void logoutClick() {
        view.showLogoutConfirmAlert();
    }

    public void logout() {
        repository.setPhoneNumber("");
        repository.setCode("");
        repository.setDeviceToken("");
        view.startNewActivity(AuthActivity.class);
    }
}
