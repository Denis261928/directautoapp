package com.vallsoft.directionautoapp.activity.main.view;

import com.vallsoft.directionautoapp.helper.RequestMainPageStatus;

public interface MainView {
    void setScreenStatusBackground(RequestMainPageStatus status);
    void setNoInternetVisible(boolean visibility);
    void showNotificationsCount(int count);
    void hideNotificationsCount();
    void startAuthActivity();
    void showNotificationCallAccepted();
    void startVibration();
    void showProgress();
    void hideProgress();
}
