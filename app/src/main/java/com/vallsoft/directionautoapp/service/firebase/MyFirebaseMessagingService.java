package com.vallsoft.directionautoapp.service.firebase;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vallsoft.directionautoapp.R;
import com.vallsoft.directionautoapp.activity.main.MainActivity;
import com.vallsoft.directionautoapp.model.NotificationMessage;
import com.vallsoft.directionautoapp.model.QuestionData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static String NOTIFICATION_CHANNEL_ID = "com.vallsoft.directionautoapp";

    private static String TAG = "MyFirebaseMessagingService";

    @SuppressLint("WrongThread")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // if you want to receive notifications in foreground, background and when app closed
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            Map<String, String> data = remoteMessage.getData();
            String title = data.get("title");
            String message = data.get("body");
            String date = data.get("date");
            boolean isRead = Boolean.parseBoolean(data.get("read"));
            String pushType = data.get("push_type");
            if (pushType != null) {
                if (pushType.equals("SHOW_POLL")) {
                    boolean multipleChoice = Boolean.parseBoolean(data.get("multiple_choice"));
                    int questionCode = Integer.parseInt(data.get("question_code"));
                    String question = data.get("poll_question");
                    String jsonArrayAnswers = data.get("poll_answers");
                    ObjectMapper mapper = new ObjectMapper();
                    List<String> answerList = new ArrayList<>();
                    try {
                        answerList = mapper.readValue(jsonArrayAnswers, List.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    QuestionData questionData = new QuestionData(isRead, multipleChoice, questionCode, question, answerList);
                    showPollNotification(title, message, isRead, questionData);
                }
            } else {
                Log.d("111111", "data");
                showMessageNotification(title, message, isRead, date);
            }
        }
    }

    public void showPollNotification(String title, String message, boolean isRead, QuestionData questionData) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    "MyNotifications", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("question", questionData);
        intent.setAction("TRANSACTIONS_CATEGORY");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                /* Request code */ NotificationsUtils.getRequestCodePush(this),
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultBoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        @SuppressLint("ResourceAsColor")
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                NOTIFICATION_CHANNEL_ID)
                .setContentTitle(title)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_logo_small)
                .setColor(getResources().getColor(R.color.colorBackgroundWhite))
                .setAutoCancel(true)
                .setContentText(message)
                .setSound(defaultBoundUri)
                .setContentIntent(pendingIntent);

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(NotificationsUtils.getNextNotificationId(this), builder.build());
    }

    public void showMessageNotification(String title, String message, boolean isRead, String date) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    "MyNotifications", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        Intent intent = new Intent(this, MainActivity.class);
        NotificationMessage notificationMessage = new NotificationMessage(title, message, isRead, date);
        intent.putExtra("message", notificationMessage);
        intent.setAction("TRANSACTIONS_CATEGORY");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                /* Request code */ NotificationsUtils.getRequestCodePush(this),
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultBoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        @SuppressLint("ResourceAsColor")
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                NOTIFICATION_CHANNEL_ID)
                .setContentTitle(title)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_logo_small)
                .setColor(getResources().getColor(R.color.colorBackgroundWhite))
                .setAutoCancel(true)
                .setContentText(message)
                .setSound(defaultBoundUri)
                .setContentIntent(pendingIntent);

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(NotificationsUtils.getNextNotificationId(this), builder.build());
    }
}
