package com.vallsoft.directionautoapp.service.firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class NotificationsUtils {
    private static final String PREFERENCE_NEXT_NOTIFICATION_ID = "PREFERENCE_NEXT_NOTIFICATION_ID";
    private static final String PREFERENCE_REQUEST_CODE_PUSH_ID = "PREFERENCE_REQUEST_CODE_PUSH_ID";

    public static int getNextNotificationId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int id = sharedPreferences.getInt(PREFERENCE_NEXT_NOTIFICATION_ID, 0) + 1;
        if (id == Integer.MAX_VALUE) { id = 0; }
        sharedPreferences.edit().putInt(PREFERENCE_NEXT_NOTIFICATION_ID, id).apply();
        return id;
    }

    public static int getRequestCodePush(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int id = sharedPreferences.getInt(PREFERENCE_REQUEST_CODE_PUSH_ID, 0) + 1;
        if (id == Integer.MAX_VALUE) { id = 0; }
        sharedPreferences.edit().putInt(PREFERENCE_REQUEST_CODE_PUSH_ID, id).apply();
        return id;
    }
}
